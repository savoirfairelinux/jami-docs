# FAQ

This is a list of frequently asked questions (FAQ), including some technical questions.

Questions from the [Forum](https://forum.jami.net/) are also added here.

<!-- TODO: Add a table of contents -->

## Basics

### What is Jami?

See the [Introduction](introduction.md).

### What does Jami mean?

The choice of the name Jami was inspired by the Swahili word [jamii](https://en.wiktionary.org/wiki/jamii) which means `community` as a noun and `together` as an adverb.
It was chosen as it reflects the vision for the project:
* a free/libre program available to all that helps bring communities together,
* is community supported, and
* respects the freedom and privacy of the users.

### How can I make a bug report?

Please see the [Bug report guide](bug-report-guide.md).

### What makes Jami different from other communication platforms?

Jami does not work like most communication platforms because it is *distributed*:

![«Image: Centralized, decentralized, and distributed network topologies»](/_static/network-topology.svg "Centralized, decentralized, and distributed network topologies")

Some of the consequences may seem surprising.
For instance, since accounts are stored on the user's device, passwords are optional.
However, the most significant practical differences are that the user havs more *freedom* and *privacy*.

<!-- TODO: Expand this -->

### What do the green/orange/red status circles next to profile pictures mean?

On a user's account, 🔴 (a red circle) displayed means that the device is not connected to the DHT or is offline.
A detailed error should be displayed explaining the issue.
Checking the network/Internet connection or restarting the app may resolve the issue.

For contacts:
* no circle displayed means that no device is detected on the DHT. The contact is unreachable.
* 🟠 (an orange circle) displayed means that at least a device is announced on the DHT, so the contact SEEMS to be reachable.
  The device does not have a direct connection towards this peer yet.
  But they should be able to receive connection requests.
  Because the device is not directly connected to anything, the device status is unable to be determine for sure.
  So this can be translated to "Seems to be reachable", but connection may fail (firewall, deadlock, NAT, etc).
  The presence generally have a [TTL](https://en.wikipedia.org/wiki/Time_to_live) of:
  * 10 minutes over the DHT, and
  * several hours if [push notifications](https://en.wikipedia.org/wiki/Push_technology#Push_notification) are enabled.
* 🟢 (a green circle) displayed means that the device is connected to a device of this peer.

### Why is a feature missing on my client?

Not every client implements all features.
Check the [All features by client](all-features-by-client.md) list to see if the client is missing the feature.
Feature requests can be made at <https://git.jami.net>.

### Does Jami support read receipts? Can I turn them on or off?

Yes.
Read receipts can be enabled or disabled in the settings on Jami for Desktop, Android, Android TV and iOS.

### Does Jami support typing indicators? Can I turn them on or off?

Yes.
Typing indicators can be enabled or disabled in the settings on Jami for Desktop, Android, and iOS.

### Can I share my screen?

Yes.
Screen sharing is available on Jami for Desktop, and Android.
To activiate screen sharing, click the "Share screen" button while in an audio or a video call.

### Can I make group conference calls?

Yes.
Jami contacts can be added to (audio or video) calls by clicking the "Add participant" button.

### Does Jami support group chats?

Yes.

### Why aren't my sent messages showing up on all linked devices?

Before the implementation of Swarm Technology, an account's devices would only receive the same messages from contacts if the device was online at the time of the message being sent.
However, *sent* messages would not show up on devices other than the one sending the message.

With the implementation of Swarm Technology, conversation histories of new conversations (including one-on-one conversations) are fully synchronized between all of an account's linked devices.
Please upgrade all old versions of Jami to the latest  version that includes Swarm Technology.
The latest version of Jami is always available from the Download page of the Jami website, at <https://jami.net/download/>.

To learn more about Swarm Technology, visit the blog post [Synchronizing conversation history with Swarm](https://jami.net/synchronizing-conversation-history-with-swarm/)
and visit the [Developer manual](/developer/index).

### Can I message offline contacts?

With Swarm Technology conversations, every device stores a copy of all of the messages in that conversation.
If a user's, or another participant's, device is unreachable when a message is sent, when it becomes reachable, it will try to fetch any new messages from other reachable devices and synchronize message history.
This can be done whenever at least one other device that has a copy of the new messages is also reachable.

```{seealso}
To read more about how conversations using Swarm Technology are synchronized, visit the blog post
[Synchronizing conversation history with Swarm](https://jami.net/synchronizing-conversation-history-with-swarm/).
```

If the participants in a conversation are often not online at the same time (for instance, due to different timezones), one of them might choose to set up Jami on an often-online device.
The often-online device would receive the messages from each participant and relay it to the other(s) whenever they come online.
Thus, acting similarly to a "server", all the while Jami remains distributed by nature.

(where_are_the_configuration_files_located)=
### Where are the configuration files located?

Jami saves its configuration (account, certificates, history) at different locations depending on the platform.

- **GNU/Linux**: global configuration is under
  `~/.config/jami/dring.yml`, and account-specific files are under
  `~/.local/share/jami/`.  Finally, there is a cache directory at
  `~/.cache/jami/`.

- **macOS**: the full configuration is under
  `~/Library/Application Support/Jami/` if installed via
  <https://jami.net>.  The app store version uses
  `~/Library/Containers/com.savoirfairelinux.ring.macos/Data/Library/Application Support/jami/`.

- **Android**: the full configuration is under `/data/data/cx.ring/`
  (may require root privileges to view or change from outside Jami).

- **Windows**: global configuration is under
  `%AppData%/Local/jami/dring.yml`, and account-specific files are
  under `%AppData%/Local/jami/`.  Finally, there is a cache
  directory at `%USERPROFILE%/.cache/jami/`.

```{note}
Audio and video messages are recorded in the local-data in the folder: `sent_data`
```

For files, if a file is saved (right-click on the file, then Save), it will be added to the directory configured in the application settings.

### How much bandwidth do I need for calls?

For audio calls, Jami uses about 100 Kbps.
For a video call, about 2 Mbit/s is required for medium quality.
If the connection is slower, the bitrate will be automatically reduced.

If a device is hosting a video conference, approximately an additional 2 Mbps per participant is required.
So, for example, for a conference with 10 participants, each participant will need 2 Mbps up and down, and the host will need 20 Mbps up and down.

Jami also uses an algorithm to change the consumption depending of the quality of the link.
So, the bitrate can have a minimum of 200 Kbit/s and maximum of 6 Mbit/s.

### How can Savoir-Faire Linux Inc. (SFL) afford to give Jami away for free? How does SFL make money with Jami?

[Savoir-Faire Linux Inc.](https://savoirfairelinux.com/) (SFL) is a consulting company with some R&D projects.
Jami is a GPLv3+ project and this will not change.
SFL already sells services for several fields of expertise (hosting, developing websites and applications, embedded software, etc).

Jami is financed several ways:
+ [Donations](https://www.paypal.com/donate?hosted_button_id=MGUDJLQZ4TP5W)
+ [Customization contracts](mailto:sales@jami.biz)
+ Services for other projects
+ Visibility
+ [R&D](https://en.wikipedia.org/wiki/Research_and_development)
+ <https://jami.biz>

Jami recurring expenses:
+ As a distributed system, Jami incurs very low costs by design
+ Opt-in collection of anonymized statistics might be added in the future to better understand Jami usage; however, no personal data will be collected.

## Account management

### What is a Jami account?

A Jami account is an [asymmetric encryption key](https://en.wikipedia.org/wiki/Public-key_cryptography).
The Jami account is identified by a Jami ID, which is a [fingerprint](https://en.wikipedia.org/wiki/Public_key_fingerprint) of the public key.

### What information do I need to provide to create a Jami account?

When a new Jami account is created, private information like an email address, address, or phone number is not required.

The following information can be provided:
1. A profile picture (optional).
2. A display name (optional), which is the name that clients will display for your contact.
   It can contain special characters.
3. A username (optional), which is a unique identifier that is directly associated with your Jami ID.
   This username→Jami ID mapping is stored on a server
   (`ns.jami.net` by default, but you can host your own).
4. A password (optional).
   This password is used to protect the account archive in the device.

```{seealso}
More information about Jami accounts is available in the
[Account management](/developer/jami-concepts/account-management.md) section of the Developer manual.
```

### Where is my Jami ID?

The Jami ID is displayed on the main page of the Jami app.
The Jami ID is a 40-character string of letters and numbers; for example: `f2c815f5554bcc22689ce84d45aefdda1bce9146`.

### Why don't I have to use a password?

On a centralized system, a password is required to authenticate with a public server where accounts are stored.
Any third-party who knows a person's password could steal the person's identity.

With Jami, the account is stored in a folder on the device.
There is no public server where Jami accounts are stored.
Hence, an account password is not required with Jami.
**The password is only used to encrypt a Jami account in order to protect the Jami account from someone who has physical access to the device.**

A password may not be wanted or needed if the device is encrypted.
Recent versions of Jami do not ask for an account encryption password by default when creating new accounts.

```{note}
Changing a password will only change the password on the current device and it's not synced (because there is no server and other devices can be offline anyway).
```

### Why don't I have to register a username?

The most permanent, secure identifier is the Jami ID.
However, these are difficult to use for some people, there is also an the option of registering a username.
Username registration requires a name server, such as Jami's default one at `ns.jami.net`.

If you don't register a username, you can still choose to register one later at any time.

If you host your own name server at `example.com`, usernames registered there can be looked up by searching for `username@example.com`.

### Can I change my username?

Usernames are unable to be changed on the default name server (`ns.jami.net`).

### What is the difference between a username and a display name?

The username can be used as an identifier.
The username points to the Jami ID, which is the permanent, secure identifier.
Usernames are unique on the name server.

A display name allows you to choose another name that identifies a Jami account to contacts.
Display names can be edited or changed at any time and only trusted peers can see them.

(how_can_i_back_up_my_account)=
### How can I back up my account?

There are two ways to back up an account:

1. Link another device to the account so the account will be on two devices.
   This option is available in the Account settings page.
2. Back up the account archive.
   More information about Jami accounts is available in the
   [Account management](/developer/jami-concepts/account-management.md) section of the Developer manual.
   Some clients allow exporting the Jami account archive from Account settings.

### Can I retrieve my username without my keys?

If the default name server at `ns.jami.net` stores a username, the username **cannot** be retrieved without the key.
There is no way to prove a username without the key.

If a different name server was used to store a username, there may be a way to move the username to a new Jami ID at the discretion of the administrator of the name server.

```{seealso}
More information about name servers is available in the
[Name Server protocol](/developer/jami-concepts/name-server-protocol.md) section of the Developer manual.
```

### Can I recover my account if I forget my password?

No.
There is no traditional account recovery process;
the user is the only person with access to the user's data.

```{admonition} Tip
Use a password manager if there is a concern about forgetting the Jami account password.
```

### What happens when I delete my account?

The Jami account is only stored on devices which have the Jami account.

The Jami account is gone and is unable to be restored if:
* there is no backup of the Jami account, and
* the Jami account is deleted from all devices.

Furthermore, nobody else can use the Jami account.

The contacts will still have the messages which were sent to them, but all public record of the account on the DHT will eventually disappear due to absence and lack of activity.

```{warning}
The default `ns.jami.net` name server **does not** delete any registered usernames -- other name servers might (not recommended), at their administrator's discretion.
So, if an account has a registered username on the default name server and the account is deleted or lost (without a backup), nobody (including the user) will be able to register a new account with that username again; thus, nobody can reach the user at that username anymore.

To avoid losing an account **please** {ref}`back it up <how_can_i_back_up_my_account>`!
```

### What happens when I link a new device?

When a device is linked to an account, the Jami account archive is put on the Jami network for a few minutes.
The Jami account is protected with a password Jami provides.

The new device receives the full account certificate with the master RSA keys, and it generates a new device key for signing/encrypting messages.

## Advanced

### What protocol does Jami use for the end-to-end encryption?

We use TLS 1.3 with a perfect forward secrecy requirement for the negotiated ciphers for calls and file transfers.
Messages are encrypted with an RSA key.

### What data passes through my machine when I participate in the Jami network?

**All these data are encrypted**.  There is:

- ICE descriptors of other Jami users (ICE is a protocol that helps establishing communication between two computers)
- certain text messages
- accounts currently being linked to a new device, as explained above.

Audio/video streams and some text messages pass through the VOIP protocol.
Text messages can be sent either via VOIP or DHT (the distributed network) depending on whether a VOIP communication channel is already open or not.

### Why am I able to communicate with myself?

Many users use Jami to transfer data from one machine to another.

### Should I enable push notifications?

Push notifications allow Jami to operate in a way more adapted to the context of mobility (energy consumption, data, …).
However, for the moment, notifications go through Google's servers, via the Firebase service.
Only one identifier is transferred and it is unusable for anyone who does not have access to the account.

### What is a bootstrap server?

A bootstrap server is the entry point of the distributed network.
To enter in a network, Jami must know one other node.
This is the role of the bootstrap.
It can be any node in the network, but, bootstrap nodes are generally always up and available.
The default one in Jami is `bootstrap.jami.net`.

### What is a TURN server? What is STUN?

A TURN server is a relay, and is generally used when two peers are unable to contact to each other due to some firewall restriction, have NAT without any opened port, and no IPv6.

A STUN server is only used for SIP accounts, and is generally used to obtain the device public IP address.
For Jami accounts, the DHT already provides the device public IP address.

### What is DHT proxy?

The DHT proxy is a server that registers on the DHT on behalf of the device and relays information to and from the device.
Thus, it is the server that will be active on the DHT and will participate in the network, and no longer the target device.
Multiple devices can register on the same DHT proxy.

Generally, to transfer data between two peers, there are 3 steps:

1. Exchange candidates (IP addresses) via the DHT
2. Negotiate the best [P2P](https://en.wikipedia.org/wiki/Peer-to-peer) channel between the peers
3. Transfer data on this socket.

The DHT is only used for the first step.

### What if I disable the DHT proxy on Android and what about push notifications?

There is basically 3 modes on how to use the Android application:

+ With push notifications (DHT proxy must be enabled).
  This mode supports notifications for Android (via Google/Firebase, and soon UnifiedPush or Apple/APN).
  This decrease battery usage, by removing the sync needed with the DHT and without any socket always alive.
+ Without push notifications but with DHT proxy enabled.
  This avoids the application synchronizing with other nodes, but "Run in the background" MUST be enabled to avoid the operating system killing the application.
+ Without DHT proxy.
  In this case, "Run in the background" MUST be enabled to avoid the operating system killing the application.
  The application will synchronize with the other DHT nodes.

### I still have issues with the Android application even if battery optimization is disabled

Please read <https://dontkillmyapp.com> for more details.
If it does not solve the issues, please open a bug report (ideally with a scenario to help reproduce and/or logs).

### How does the username registration service work?

With the default name server (`ns.jami.net`), the usernames are registered on an Ethereum blockchain.
It is possible to develop a name server with any underlying data storage technology.
For example, an SQL database could be used instead of a blockchain for the data storage technology.

With the default name server, look up usernames with `https://ns.jami.net/name/test`, where `test` is a username for which we are looking for a matching
[Infohash](/developer/jami-concepts/jami-identifiers.md).

Once registered, the name server **does not** provide any way to remove the mapping.

More information about name servers is available in the
[Name Server protocol](/developer/jami-concepts/name-server-protocol.md) section of the Developer manual.

### How can I change the timeout for a call?

In the `dring.yml` file (see {ref}`where_are_the_configuration_files_located`), the `ringingTimeout` value, measured in seconds, can be changed.

### How to back up and reimport conversations and accounts

```{note}
This is only applicable for Desktop clients.
Desktop clients run on GNU/Linux, macOS, and Windows operating systems.
```

1. Export each accounts.
   (For GNU/Linux: `Open settings` → `Account` → `Manage account` → `Backup account`).

2. Copy and save the database (in `~/.local/share/jami/` for example).

3. On the new device, to import the settings and contacts with empty conversations:
    * if Jami is opened for the first time, import the archive backup.
      `I already have an account` → `Import from an archive backup`.
    * if Jami already has an account, import each archive backup.
      `Add another account` → `I already have an account` → `Import from an archive backup`.

4. Close Jami and replace the database with the database previously saved.

### How secure are you?

**TLS/SRTP is used to secure connection and communications over the network.**

SRTP over SIP is implemented using recommendations described in the following two RFCs:

- [RFC 3711](https://tools.ietf.org/html/rfc3711)
- [RFC 4568](https://tools.ietf.org/html/rfc4568)

Typically 2 kinds of sockets are negotiated.
One for the control socket, the other for the media sockets.

Typical control session will use the following cipher suite:
```
(TLS1.3)-(ECDHE-SECP384R1)-(RSA-PSS-RSAE-SHA384)-(AES-256-GCM)
(TLS_ECDHE_RSA_AES_256_GCM_SHA384)
```

DTLS (fallback) supported:
```
"SECURE192:-KX-ALL:+ANON-ECDH:+ANON-DH:+SECURE192:-VERS-TLS-ALL:+VERS-DTLS-ALL:-RSA:%SERVER_PRECEDENCE:%SAFE_RENEGOTIATION"
```

TLS:
```
"SECURE192:-KX-ALL:+ANON-ECDH:+ANON-DH:+SECURE192:-RSA:-GROUP-FFDHE4096:-GROUP-FFDHE6144:-GROUP-FFDHE8192:+GROUP-X25519:%SERVER_PRECEDENCE:%SAFE_RENEGOTIATION"
```

Supported crypto suite for the media session are:
- `AES_CM_128_HMAC_SHA1_80 / SRTP_AES128_CM_HMAC_SHA1_80`
- `AES_CM_128_HMAC_SHA1_32 / SRTP_AES128_CM_HMAC_SHA1_32`

### When do public IPs get exposed?

We can consider three main connectivity scenarios:
(1) a classic configuration,
(2) behind a VPN, and
(3) via Tor.

As Jami is a [P2P](https://en.wikipedia.org/wiki/Peer-to-peer) application, the reader would probably know that (2) or (3) is mandatory to avoid IP leaking.

Moreover, even if it's my answer, you can choose to not trust my answer and check the code, or use wireshark or other tools.
Generally, Jami developers use the first scenario (sometimes the second one).
It is impossible to test all the possible networks configurations, so if you discover a bug, please [open an issue](bug-report-guide.md).

For all three scenarios, there are three main actions:
- sending a message (this will use the DHT);
- sending a file (TCP ICE connection as described in the
  [File transfer](/developer/jami-concepts/file-transfer.md) section of the Developer manual; and
- placing a call (TCP + UDP ICE connection as described in the
  [Calls](/developer/jami-concepts/calls.md) section of the Developer manual.

#### (1) A classic configuration

##### Send a message

The Jami application is running a DHT (<https://opendht.net>) node on your device.
So every operations on the DHT will use your IP address.
This is why Jami has the option to use a dhtproxy (eg dhtproxy.jami.net), this will avoid to use your node, but will use another node on the network (which will see your IP address).
Note that your message is not sent directly to the other device.
In fact your message is sent on some nodes of the DHT and your contact will retrieve the message on this node.
So, your contact don't see your IP address at this step, but the node who get the message will (or they will see the IP address of the proxy).

##### Send a file

As described in the docs, you will send a message with all the IP address you know that your peer can contact in an encrypted packet.
So, if your peer send you a file or you send a file, your addresses will appear in the ICE message.

##### Calls

Same as above, the IP address is present in the ICE.

#### (2) Behind a VPN

##### Send a message

The IP of your VPN will be used by the DHT node.
If you want a proof, you can compile dhtnode and run the `la` command to get your public detected address.
This is what I got:

```
./tools/dhtnode -b bootstrap.jami.net
Bootstrap: bootstrap.jami.net:4222
OpenDHT node be58fdc9f782269bfc0bbfc21a60bca5f02cb881 running on port 54299
(type 'h' or 'help' for a list of possible commands)

>> la
Reported public addresses:
IPs OF MY VPN
```

So, if you don't use a proxy, your VPN addresses will be used for using
the DHT. If you use a dhtproxy, the dhtproxy will see your VPN addresses

##### Send a file

Same as above, the ICE contains:
+ addresses from your LAN
+ public address of your VPN
+ TURN address if TURN is enabled

##### Do a call

Same as above, your public address is replaced by your VPN address.
You can see it in the logs from daemon.
See {ref}`user/bug-report-guide:logs`.

#### (3) Tor

##### Send a message

Tor basically does not supports UDP.
This means that you can not use your DHT node locally, you MUST use a DHTProxy.
That proxy will see the Exit node.

##### Send a file

I prefer a proof that any description.
So, I did a file transfer with Jami + TOR.
This is what I see in the logs for the remote:

```
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: Hc0a8c801 1 TCP 2130706431 192.168.200.1 33293 typ host tcptype passive
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: Hc0a8c801 1 TCP 2130706431 192.168.200.1 9 typ host tcptype active
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: Hc0a80103 1 TCP 2130706431 192.168.1.3 33293 typ host tcptype passive
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: Hc0a80103 1 TCP 2130706431 192.168.1.3 9 typ host tcptype active
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: R33fe279d 1 TCP 16777215 51.254.39.157 27427 typ relay tcptype passive
[1574218330.556|10688|p2p.cpp           :241  ] [Account:93a03f519f394143] add remote ICE candidate: Sc0a8c801 1 TCP 1694498815 185.220.101.24 33293 typ srflx tcptype passive
```

The first ones are some 192.168.x.x so we don't care. 51.254.39.157 is the TURN address in France (my device is in the Canada).
185.220.101.24 is the Tor exit node:

```
inetnum:        185.220.101.0 - 185.220.101.127
netname:        MK-TOR-EXIT
```

##### Do a call

This will not work (actually, you can create the SIP control connection
because it's a TCP connection), but medias are negotiated in UDP, so
this will fail.

### What ports does Jami use?

Jami works as a server and gets new ports for each connections (randomly
bound). These are the ranges that can be used for each component:

-  dht: UDP [4000, 8888]
-  audio: UDP [16384-32766]
-  video: UDP [49152-65534]
-  SIP Control: UDP/TCP randomly bound

```{note}
If UDP is blocked, a dhtproxy can be used to use TCP instead.
However, media will not work because it only supports UDP.
```

So for ufw, we recommend running `sudo ufw default allow outgoing`.

For now, you can not specify a specific range to configure ports used by
Jami. The inbound traffic can be controlled without issue, Jami should
work and can use a TURN server if needed.

If you run your own proxy or name server:

-  dhtproxy, name server: TCP [80-100], 443

If you run your own TURN server:

-  TURN/STUN: TCP+UDP 3478, 5349

### Can I use Jami in a local network (LAN) without Internet access?

Yes!
Thanks to Jami's architecture, Jami users on a local/private network can communicate among themselves using Jami, without requiring any outside connectivity such as the Internet.

To do so, from Jami's `Account` settings open `Advanced account settings`.
There, enable the `Enable local peer discovery` setting.
Additionally, you may want to manually set the `bootstrap` node's address (default: `bootstrap.jami.net`) to the IP address of another device on your network that also runs Jami and/or an OpenDHT node.

```{note}
If you will use this Jami account for communicating only with only with other devices on the same local/private network, you can disable TURN if you wish.
If you do so, and later you decide to use this account also for communicating with other Jami devices outside your network, don't forget to enable TURN again, as it helps Jami work around issues with some overly restrictive firewalls.
```

### How can I configure the codecs even more?

Codecs can be configured via a file. In the configurations files, you
can create a file called `encoder.json` like this:

```
{
    "libx264": {
        "profile": 100,
        "level": 42,
        "crf": 20,
        "preset": "ultrafast"
    },
    "h264_vaapi": {
        "low_power": 1
    },
    "libopus": {
        "application": "voip"
    }
}
```

or:

```
{
    "libopus": {
        "bit_rate": 128000
    }
}
```

This file is {ref}`located in the same directory <where_are_the_configuration_files_located>` as `dring.yml`.

To check which options are supported, use the command `ffmpeg -h
encoder=[encoder_name]`, where `encoder_name` can be any of `libx264`, `libvpx`, `mpeg4`, `h263`, `libopus`, `libspeex`, `g722`, `pcm_alaw`, or `pcm_mulaw` (the FFmpeg names for all of Jami's supported encoders).

### How can I configure the audio processor?

An audio processor allows Jami to clean up and process your microphone's audio.
It can remove echo, reduce noise, and equalize your microphone's volume.
Additionally, it can detect when you're speaking and send this information to participants in your call.
The audio processor settings can be set in your `dring.yml` file.
See {ref}`this section to find where this file is located <where_are_the_configuration_files_located>`.

The relevant preference keys are:

- `audioProcessor`, which configures which audio processor to use. The valid options are:

  - `webrtc`: the `WebRTC Audio Processing library <https://www.freedesktop.org/software/pulseaudio/webrtc-audio-processing/>`_
  - `speex`: the `Speex DSP library <https://gitlab.xiph.org/xiph/speexdsp>`_
  - `null`: disables audio processing (though your system echo canceller may still be used, see below)

- `echoCancel`, which configures how echo cancelling should be done. The valid options are:

  - `auto`: try to use your operating system's echo canceller (if it exists), otherwise fall back to the chosen audio processor's echo canceller
  - `audioProcessor`: only use the chosen audio processor's echo canceller
  - `system`: only use your operating system's echo canceller
  - `null`: don't do any echo cancelling

- `noiseReduce`, `true`/`false` to set noise reduction on the audio processor
- `automaticGainControl`, `true`/`false` to set automatic gain control on the audio processor
- `voiceActivityDetection`, `true`/`false` to set voice activity detection on the audio processor