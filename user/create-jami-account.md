Create Jami account
===================

CREATE A JAMI ACCOUNT ON: 

Android (EN) [create-jami-account-android-en.pdf](create-jami-account-android-en.pdf)

Android (FR) [creer-compte-jami-android-fr.pdf](creer-compte-jami-android-fr.pdf)

macOS (EN) [create-jami-account-macos-en.pdf](create-jami-account-macos-en.pdf)

macOS (FR) [creer-compte-jami-macos-fr.pdf](creer-compte-jami-macos-fr.pdf)

iOS (EN) [create-jami-account-ios-en.pdf](create-jami-account-ios-en.pdf)

iOS (FR) [creer-compte-jami-ios-fr.pdf](creer-compte-jami-ios-fr.pdf)

Windows (FR) [creer-compte-jami-windows-fr.pdf](creer-compte-jami-windows-fr.pdf)
