# Use Jami on a LAN

Due the distributed nature of Jami, it is possible to use it over a [local area network (LAN)](https://en.wikipedia.org/wiki/Local_area_network), without any Internet connection.
This allows you to continue to communicate with other people in the company/building/country without global Internet access.
However, some services are external so in this document we will explain some tweaks you may need.

```{contents}
   :local:
   :depth: 3
```


## Bootstrapping


### What is a bootstrap?

Jami uses the [distributed hash table (DHT)](https://en.wikipedia.org/wiki/Distributed_hash_table) technology to find other users.
So, all the users you want to contact must be reachable on the same DHT network (e.g. if the Internet is split between two buildings, users in the first buildings will not be able to reach the second building).
To enter a DHT network, one must be able to reach at least one other node.
This node is called bootstrap, it is the entry point of the network.
By default, **bootstrap.jami.net** is configured, but any node in the public DHT can be a bootstrap (it is a classic node, generally always online).

So, if the Internet is cut, you need another bootstrap to create a network.
On a LAN, there is two ways to configure it in Jami:


### Bootstrap settings

In the advanced account settings, the user can configure multiple bootstraps.
**bootstrap.jami.net** is usually the default, **bootstrap.jami.net;your.bootstrap.tld** will be valid.
The IP:port of another DHT node can be specified as a bootstrap.


### Running a bootstrap

It's possible to run a DHT node to serve as a bootstrap for Jami clients.
In this case, the bootstrap field in the settings must be replaced by the new bootstrap.
The documentation to run a DHT node is located in OpenDHT's wiki:
<https://github.com/savoirfairelinux/opendht/wiki/Running-a-node-with-dhtnode>.


### Local Peer Discovery

Another way is to enable peer discovery.
This will announce the bootstrap on the network by broadcasting UDP packets (like a printer).
So, UDP broadcast must be supported by the network in order to work.
However, this method does not need to specify an ip:port in the settings, so it can be preferred.


## TURN

Another external service is the TURN server, used as a fallback for connections if the NAT block all possible connections.
Generally it is **turn.jami.net** but can be any TURN (we use [coturn](/developer/going-further/setting-up-your-own-turn-server)).

On a LAN, it may be ignored (because there will be no NAT), but disabling it should not be necessary (because it will not be used if unreachable).


## On mobile (DHT Proxy)

A DHT Proxy is used with mobile devices to save battery by avoiding synchronization.
It is generally **dhtproxy.jami.net** but can be any DHT node with the REST API enabled.
However, if the DHT proxy is using push notifications it will depend on another external service (Firebase or APN or a UnifiedPush instance).
In this case, only the third one can be self-hosted.

On iOS it is basically impossible to work without push, as the OS from Apple will kill any application as soon as it is in background.
So you can not disable the usage of push-notifications.
However, for Android, you may want to self-host your proxy (with or without UnifiedPush support), or you can disable the DHT Proxy and enable "Run in the background" in order to use your local DHT.


## Name server

Finally, the last external service you may need is a **name server**.
This is used to translate addresses (the 40-characters fingerprint ID) to usernames.
You may not have access to **ns.jami.net**, but you can self-host one (:doc:`/developer/name-server-protocol`) or only use IDs.