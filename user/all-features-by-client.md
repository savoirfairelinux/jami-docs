# All features by client

## Legend

| Key     | Description                                                 |
|:-------:|:------------------------------------------------------------|
| ✓       | Available                                                   |
| 🧪      | Experimental                                                |
| ×       | Not yet available                                           |
| 🚫      | Not applicable for this client                              |
| ?       | Unknown/under development                                   |
| Desktop | Apple macOS, GNU/Linux, Microsoft Windows operating systems |
| iOS     | iPad and iPhone devices with iOS operating system           |
| Web     | Jami access with a web browser                              |

## Text messaging

| Client                             | Desktop | Android | Android TV | iOS | Web |
|:-----------------------------------|:-------:|:-------:|:----------:|:---:|:---:|
| Text messaging                     |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Group chat                         |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Public chat                        |    ×    |    ×    |     ×      |  ×  |  ?  |
| Chat history shared across devices |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Send files                         |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Auto accept images                 |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Custom download location           |    ✓    |    ×    |     ×      |  ×  |  ✓  |
| Typing indicator                   |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Read status                        |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Reply to                           |    ✓    |    ✓    |     ×      |  ×  |  ✓  |
| Message edition                    |    ✓    |    ✓    |     ×      |  ×  |  ✓  |
| Emoji reaction                     |    ✓    |    ✓    |     ×      |  ×  |  ✓  |
| Update conversation's profile      |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Add/Kick members                   |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Conversation's preferences         |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Search messages                    |    ✓    |    ✓    |     ×      |  ×  |  ×  |
| Share messages                     |    ✓    |    ✓(1) |     ×      |  ×  |  ✓  |
| Share location                     |    ✓    |    ✓    |     ×      |  ✓  |  ×  |
| Media gallery                      |    ✓    |    ✓    |     ×      |  ×  |  ×  |

(1) Sharing messages and media is available on Android through the system-native sharing feature

## Calling

| Client                             | Desktop | Android | Android TV | iOS  | Web |
|:-----------------------------------|:-------:|:-------:|:----------:|:----:|:---:|
| Audio calls                        |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Video calls                        |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Group calls                        |    🧪   |    ✓    |     🧪      |  ×   |  ×  |
| Host call conference               |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Auto bitrate                       |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Change video quality               |    ✓    |    ✓    |     ✓      |  ×   |  ×  |
| Custom ringtones                   |    ✓    |    ✓    |     ×      |  ×   |  ×  |
| Select camera                      |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Video call recording               |    ✓    |    ×    |     ×      |  ×   |  ×  |
| Leave audio message                |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Leave video message                |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Screen sharing                     |    ✓(1) |    ✓    |     ×      |  ×   |  ×  |
| Window sharing                     |    ✓(3) |    ×    |     ×      |  ×   |  ×  |
| Media sharing                      |    ✓    |    ×    |     ×      |  ×   |  ×  |
| Hardware encoding\*                |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Hardware decoding\*                |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Mute sound                         |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Texting while on call              |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Remote recording notification      |    ✓    |    ×    |     ×      |  ✓   |  ×  |
| Rendez-vous mode                   |    ✓    |    ×    |     ✓      |  ×   |  ✓  |
| Conference layout                  |    ✓    |    ✓    |     ×      |  ✓   |  ×  |
| Multistream                        |    ✓    |    ×    |     ×      |  ×   |  ×  |
| Push-to-talk                       |    ✓(1) |    ×    |     ×      |  ×   |  ×  |
| Videosplit                         |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |
| Audio processor\*\*                |    ✓    |    ✓(2) |     ✓(2)   |  ✓(2)|  ×  |
| Voice activity                     |    ✓    |    ✓    |     ✓      |  ✓   |  ×  |

\* This enables 4K video calling support<br>
\*\* cf. {ref}`user/faq:How can I configure the audio processor?` in the FAQ

(1) Available on both X and Wayland<br>
(2) Some features enabled by default and not configurable<br>
(3) Only on X and not on Microsoft Windows

## Account settings

| Client                      | Desktop | Android | Android TV | iOS | Web |
|:----------------------------|:-------:|:-------:|:----------:|:---:|:---:|
| Profile picture             |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Disable account             |    ✓    |    ✓    |     ×      |  ✓  |  ×  |
| Delete account              |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Contact availability        |    ✓    |    ✓    |     ✓      |  ✓  |  ×  |
| Register username           |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Delete contacts             |    ✓    |    ✓    |     ✓      |  ✓  |  ✓  |
| Block contacts              |    ✓    |    ✓    |     ×      |  ✓  |  ✓  |
| Link new device via DHT     |    ✓    |    ✓    |     ✓      |  ✓  |  ×  |
| Link new device via archive |    ✓    |    ✓    |     ×      |  ×  |  ×  |
| Auto answer                 |    ✓    |    ✓    |     ✓      |  ×  |  ×  |
| Custom ringtones            |    ✓    |    ✓    |     ×      |  ×  |  🚫 |

## Other features

| Client                         | Desktop | Android | Android TV | iOS | Web  |
|:-------------------------------|:-------:|:-------:|:----------:|:---:|:----:|
| Scan QR code                   |    ×    |    ✓    |     ×      |  ✓  |  ✓   |
| Display QR code                |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| Block contact                  |    ✓    |    ✓    |     ×      |  ✓  |  ✓   |
| System notifications           |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| Supported languages\*          |   106   |   106   |    106     | 106 |  106 |
| Contacts shared across devices |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| Multi account                  |    ✓    |    ✓    |     ×      |  ✓  |  ✓   |
| SIP account                    |    ✓    |    ✓    |     ×      |  ✓  |  ×   |
| SIP transferring               |    ×    |    ×    |     ×      |  ×  |  ×   |
| Dark theme support             |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| JAMS\*\* support               |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| Plugin support                 |    ✓    |    ✓    |     ✓      |  ×  |  ×   |
| Plugin store                   |    ✓    |    ×    |     ×      |  ×  |  ×   |
| Application translation        |    ✓    |    ✓    |     ✓      |  ✓  |  ✓   |
| White labeling                 |    ✓    |    ✓    |     ×      |  ×  |  ×   |
| Oauth2 support                 |    ×    |    ×    |     ×      |  ×  |  ✓   |


\* According to <https://explore.transifex.com/savoirfairelinux/jami/><br>
\*\* JAMS (Jami Account Management Server) <https://jami.biz/>

## Advanced settings

| Client             | Desktop | Android | Android TV | iOS | Web |
|:-------------------|:-------:|:-------:|:----------:|:---:|:---:|
| DHT proxy support  |    ✓    |    ✓    |     ×      |  ✓  |  🚫 |
| Push notification  |    🚫   |    ✓    |     🚫     |  ✓  |  🚫 |
| UPnP               |    ✓    |    ✓    |     ✓      |  ×  |  🚫 |
| TURN configuration |    ✓    |    ✓    |     ✓      |  ✓  |  🚫 |
