```{important}
We are currently a small number of developers active on the project.
As such, we cannot answer and tag all of the opened issues immediately, but we do notice and read them.
Good bug reports provide us important feedback, which we thank you for and always appreciate.
```

# Bug report guide

This guide includes step-by-step instructions for reporting bugs and
issues you encounter in Jami.

## Set up your environment

- Be ready for data loss.  Back up your account and link your account
  to as many devices as possible.
- Install the latest version (or even a beta version) of Jami.
  Reporting bugs/issues against older versions is less useful, and
  there is a likelihood of it already having been fixed in newer
  versions.

## How to report a bug

0. Create an account on the
[Jami GitLab](https://git.jami.net/users/sign_up) if you don't have one already.

1. Choose the right project to post your issue in:
   * {gitlab-project}`The Android client <jami-client-android>`
   * {gitlab-project}`The Qt client <jami-client-qt>`
   * {gitlab-project}`The iOS client <jami-client-ios>`
   * {gitlab-project}`The macOS client <jami-client-macos>`
     (deprecated by {gitlab-project}`the Qt client <jami-client-qt>`)
   * {gitlab-project}`The Jami project in general (or if you are not sure) <jami-project>`
   * [If you know what you are doing you may choose one of the other projects](https://git.jami.net)

2. If you have multiple issues, please file separate bug reports.
   It will be much easier to keep track of them that way.

3. The title is an explicit summary of the bug (e.g.: header bar is
   too big due to icon size)

4. Figure out the steps to reproduce the bug:
   * If you have precise steps to reproduce it (great!) you're on your way to creating a helpful bug report.
   * If you can reproduce occasionally, but not after following specific steps, please provide additional information about the issue to help others understand and try to reproduce it.
   * If you can not reproduce the problem, there may be little chance of it being reasonably fixable.
     If you do report it, please try your best to provide as much information/clues about its occurrence as possible.

5. Make sure your software is up to date.  Ideally, test an
   in-development version to see whether your bug has already been fixed.

6. Try to isolate from the environment and reproduce (i.e. test on
   multiple devices).

7. Describe your environment(s) by specifying the following:
   * OS version
   * precise device model (important for mobile devices)
   * if you are using a beta version
   * what build you are using (F-Droid, Play Store, App Store, from `dl.jami.net`, your own build, etc.).
     If you have built your own version of Jami, please specify the exact Jami Daemon version and client version (you can obtain it using `jamid -v` and `jami -v`;
     but note that our packages are updated quite often) and the Git commit.
   * network conditions:
     * are both devices on the same local network?
     * Different networks?
     * Is one or both behind NAT?
     * Are you using LTE?
     * Are you using WiFi?
   * other elements if needed:
     * SIP provider,
     * hardware,
     * etc.

## Writing a clear summary

How would you describe the bug using approximately 10 words?
This is the first part of your bug report a developer will see.

A good summary should quickly and uniquely identify a bug report.
It should explain the problem, not your suggested solution.

```
Good: "Cancelling a file transfer crashes Jami"
Bad: "Software crashes"
```

```
Good: "All calls hang up after 32 seconds"
Bad: "Not able to call my friends"
```

## Writing precise steps to reproduce

* How can a developer reproduce the bug on his or her own device?

  Steps to reproduce are the most important part of any bug report.
  If a developer is able to reproduce the bug, the bug is very likely
  to be fixed.  If the steps are unclear, it might not even be
  possible to know whether the bug has been fixed.  We are totally
  aware that some bugs may look obvious to you, but they are probably
  related to your environment.  The more precise you are, the quicker
  the bug can be fixed.

* What should you include in a bug report?

  Indicate whether you can reproduce the bug at will, occasionally,
  or not at all.  Describe your method of interacting with Jami in
  addition to the intent of each step.  After your steps, precisely
  describe the observed (actual) result and the expected result.
  Clearly separate facts (observations) from speculations.

### Good

I can always reproduce by following these steps:

<pre>
1. Start Jami by clicking on the desktop icon
2. Start a new conversation with anyone
3. Click the file transfer icon

Expected results: A window opens and asks me to choose a file to send.
Actual results: When I click the file transfer icon, nothing happens.
</pre>

### Bad

<pre>
Try to transfer a file.
It doesn't work.
</pre>

## Obtained Result

Please include:

* The Jami daemon (jamid or libjami or libring) and client debug logs.
* The core dump if one was produced.

## Expected Result

It's a description of expected or desired behavior.

## Providing additional information

The following information is requested for most bug reports.  You can
save time by providing this information below the Expected results.

### Logs

#### Qt-based Jami client (GNU/Linux, Windows, macOS)

Go to the General settings.
In the Troubleshoot section, you can click on "Open logs", where you will be able to get statistics ("Show stats") or start recording information via "Receive logs".
Then you can just copy the result and explain your scenario.

#### On GNU/Linux

Classic logs (by default logs only >= warning are logged):

```
journalctl --since "24h ago" | grep jami
```

Full log:
Since the Jami client (GUI) and daemon are separated processes, the easiest way to get logs from both is to start them one at a time, manually.

1. Ensure that no Jami client or daemon instances are running: check
   by running `ps aux | grep jami` in a terminal.
   * Jami may still be running even if no windows are open, depending on your preferences.
   * If either client or daemon are running, terminate them using `kill PID`.
2. In one terminal, start the daemon with `jamid -d -c`
   * This executable is normally not in the `PATH`, and in the Debian/Trisquel/Ubuntu packages, it is located at
     `/usr/lib/x86_64-linux-gnu/jamid -d -c` or
     `/usr/libexec/jamid -d -c`.
3. In another terminal, start the client, using `jami -d`.

To get a backtrace, you can run the program inside GDB:

`gdb -ex run --args jami -d`, or
`gdb -ex run --args /usr/libexec/jamid -cd`, depending on the
component you need to debug.

When it crashes, you can type `bt` (or even better, `thread apply all bt`) then press *Enter*.
Then copy the backtrace and paste it in the issue.

#### On macOS

* Navigate to `/Applications/Jami.app/Contents/MacOS/`.
* Double click Jami.  It will launch Jami and print the log to the terminal.
* Copy the log from terminal to a file.

Alternatively, you could run `/<path to Jami>/Jami.app/Contents/MacOS/Jami -d` from the terminal.

#### On Android

To gather logs via your phone using Jami itself:

* Tap `Conversations`
* Tap the three-dot menu on the top right
* Tap `Settings`
* Tap `Diagnostic logs`

To gather logs via your computer using the Android Debug Bridge (adb):

* You need to have adb set up on your computer.
* Launch Jami on your smartphone and then execute
* ```adb logcat *:D | grep `adb shell ps | egrep 'cx.ring' | cut -c10-15` > logring.txt```
* You now have a file containing the log of the client

#### For Windows

Open a terminal (cmd.exe) and launch Jami.exe with the following options:

* `-d` to open a separate console window to receive logs
* `-f` to write logs to `%localappdata%\jami\jami.log`