# User manual

<!-- TODO: [Use your existing Jami account](use-existing-jami-account) -->
<!-- TODO: [Configure your SIP account](sip-account) -->
<!-- TODO: [Manage your contacts](manage-contacts) -->
<!-- TODO: [Establish communications](calls) -->
<!-- TODO: [Jami account configuration](account-configuration) -->

The Jami user manual introduces Jami and its features, provides
additional information on methods of installing Jami, answers users'
most common questions in the FAQ (frequently asked questions), and
includes various user guides and tutorials for users, such as the
[Bug report guide](bug-report-guide.md) with step-by-step instructions for reporting
bugs and issues.


```{toctree}
:maxdepth: 1
introduction
create-jami-account
jami-distributed-network
all-features-by-client
faq
lan-only
bug-report-guide
```