# Home

![«Image: Jami logo»](_static/logo-jami.svg "Jami logo")

This is the documentation for [Jami](https://jami.net), free/libre software for [universal communication](user/introduction.md) that respects the freedom and privacy of its users.
Jami is available across operating systems and platforms including GNU/Linux, macOS, Windows, Android/Replicant, and iOS.

Jami is an official GNU package and you can redistribute it and/or modify it under the terms of the [GNU General Public License](https://www.gnu.org/licenses/gpl.html) as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.
The Jami team [uses Gerrit for development](developer/new-developers/working-with-gerrit.md) and code reviews, and issues and feature requests are [tracked on the Jami GitLab](user/bug-report-guide.md).
The development of Jami is led by [Savoir-faire Linux Inc.](https://savoirfairelinux.com/en) --- a Canadian GNU/Linux consulting company based in Montréal, Québec --- and is supported by a growing global community of users and contributors.

This documentation is community-driven and [anyone can contribute](contribute.md#contributing-to-this-documentation)!

```{seealso}
You may also be interested in the daemon's [code coverage](https://docs.jami.net/coverage/),
or the [OpenDHT wiki](https://github.com/savoirfairelinux/opendht/wiki).
```

## Documentation manuals

Here are the documentation manuals of Jami and details on how to
contribute to Jami:

```{toctree}
:maxdepth: 1

user/index
jams/index
build/index
developer/index
extension/index
contribute
```

## Community and support

Learn more about:

* Jami: <https://jami.net/>
* Jami extensions: <https://jami.net/extensions/>
* “Jami Account Management Server” (JAMS): <https://jami.biz/>
* Jami documentation: <https://docs.jami.net/>

Follow us for more:

* Mastodon: [@Jami@mstdn.io](https://mstdn.io/@Jami)
* X: [@jami_social](https://x.com/jami_social)
* YouTube: [@jami9311](https://www.youtube.com/@jami9311)

We’d love to hear from you! Join the Jami community:

* Contribute: <https://jami.net/contribute/>
* Forum: <https://forum.jami.net/>
* GNU Mailman: [jami@gnu.org](https://lists.gnu.org/mailman/listinfo/jami)
* Libera.Chat: [#jami](https://web.libera.chat/#jami)
* Matrix: [#jami:matrix.org](https://matrix.to/#/#jami:matrix.org)
  (bridged with Libera.Chat)

You can also contact the Jami team at Savoir-faire Linux Inc. directly via
email at <contact@jami.net>.  Additionally, Savoir-faire Linux Inc. offers
commercial support for Jami via <https://jami.biz>.