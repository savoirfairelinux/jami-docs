# Introduction

A Jami installation typically has two main components:

1. the Jami daemon/library, and
2. the client (i.e., front-end or user interface).

To use Jami, the Jami daemon/library is always needed, since it is the core of Jami and contains all of the connectivity, communication, cryptography, and media logic.
It uses libraries such as OpenDHT, PJSIP, GnuTLS, and FFmpeg and has several APIs, including DBus, libwrap (shared library), JNI, and REST.
These APIs make it possible to interact with Jami without going through a graphical user interface (especially useful for using Jami in a headless/server setting), build automation tools/scripts around Jami, and build custom user interfaces for Jami.

The client (i.e., user interface) depends on the operating system and/or platform being used.
For example, on Android/Replicant systems this would be [jami-client-android](https://git.jami.net/savoirfairelinux/jami-client-android),
on iOS systems this would be [jami-client-ios](https://git.jami.net/savoirfairelinux/jami-client-ios),
and on GNU/Linux, macOS, and Windows this would be [jami-client-qt](https://git.jami.net/savoirfairelinux/jami-client-qt).
There also used to be a GTK-based jami-client-gnome for GNU/Linux and a jami-client-macos for macOS, both of which were deprecated in favor of the newer cross-platform jami-client-qt based on the Qt framework.

On GNU/Linux systems, jami-client-qt can be configured and built to use one of two main APIs for communicating with the Jami daemon/library:

1. libwrap: When jami-client-qt is configured to use libwrap (which is always the case on macOS and Windows), it will use Jami daemon's shared library API, and there will be no separate daemon process.
   This has the advantage of things being somewhat more efficient than with the DBus API mentioned below, in exchange for less flexibility (not being able to interact with Jami via DBus when using libwrap).

   The Jami packages distributed via dl.jami.net are currently all configured to use the libwrap API.

2. DBus: When jami-client-qt is configured to use DBus, it will communicate with the Jami daemon via its DBus API, and the daemon will be running as a separate process by itself.
   This is greatly flexible for communicating with and controlling the daemon through other means (for example, small utility scripts) simultaneously while the Qt-based Jami client also uses and interacts with it.
   The associated cost of this added flexibility is the overhead of using DBus.