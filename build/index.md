# Build manual

The Jami build manual documents the various aspects of building Jami from source.
It includes how to package Jami for various systems.

This manual is for anyone looking to build Jami from source to:
* Hack on Jami and contribute to its development;
* Test new and unreleased features; and
* Package or maintain a Jami package in a GNU/Linux distribution repository.

## Sections

```{toctree}
:maxdepth: 1

introduction
dependencies
```