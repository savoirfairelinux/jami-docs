# Dependencies

Jami is a large and complex piece of software, and depends on several external free software libraries.
The dependencies and how to install them on some systems are outlined below.

[//]: # (TODO: Distinguish between build time and runtime dependencies?)
[//]: # (TODO: Provide separate instructions for dnf and zypper?)

## jami-daemon

Building the Jami daemon requires the following tools and libraries:

- GNU Autotools (autoconf, autoconf-archive, autopoint, and automake)
- GCC (GNU Compiler Collection) C++ compiler (g++)
- [GNU Make](https://www.gnu.org/software/make/manual/)
- [GNU gettext](https://www.gnu.org/software/gettext/)
- [GNU Libtool](https://www.gnu.org/software/libtool/)
- [GnuTLS](https://gnutls.org/)
- [Argon2](https://en.wikipedia.org/wiki/Argon2)
- [asio](https://en.wikipedia.org/wiki/Audio_Stream_Input/Output)
- [dbus](https://www.freedesktop.org/wiki/Software/dbus/)
- dbus-c++
- [libupnp](https://github.com/pupnp/pupnp)
- [libssl](https://www.openssl.org/)
- [libpulse](https://gitlab.freedesktop.org/pulseaudio/pulseaudio)
- [libasound2](https://github.com/pop-os/libasound2)
- [libexpat](https://libexpat.github.io/)
- [pcre3](https://www.pcre.org/)
- [yaml-cpp](https://github.com/jbeder/yaml-cpp)
- [libXext](https://gitlab.freedesktop.org/xorg/lib/libxext)
- libxfixes
- [speex](https://www.speex.org/)
- speexdsp
- uuid
- [FFmpeg](https://ffmpeg.org/) (libavcodec, libavdevice, libswscale, and libavutil)
- [opus](https://opus-codec.org/)
- [udev](https://en.wikipedia.org/wiki/Udev)
- [fmt](https://github.com/fmtlib/fmt)
- gsm
- [pjproject](https://www.pjsip.org/) (custom Jami fork required)
- [jsoncpp](https://github.com/open-source-parsers/jsoncpp)
- [libarchive](https://libarchive.org/)
- [libnatpmp](https://github.com/miniupnp/libnatpmp)
- [libva](https://github.com/intel/libva)
- [libcrypto++](https://github.com/weidai11/cryptopp)
- [libvdpau](https://en.wikipedia.org/wiki/VDPAU)
- [libsecp256k1](https://github.com/bitcoin-core/secp256k1)
- [libgit2](https://libgit2.org/)
- [http-parser](https://pub.dev/packages/http_parser)
- [nasm](https://www.nasm.us/)
- [yasm](https://yasm.tortall.net/)
- [Nettle](https://www.lysator.liu.se/~nisse/nettle/)
- [OpenDHT](https://github.com/savoirfairelinux/opendht)
- [RESTinio](https://github.com/Stiffstream/restinio)
- [webrtc-audio-processing](https://github.com/tonarino/webrtc-audio-processing)
- [zlib](https://zlib.net/)

On dpkg/Debian-based GNU/Linux distributions
(such as [Debian](https://www.debian.org/), [Trisquel](https://trisquel.info/), [Ubuntu](https://ubuntu.com/), and [Linux Mint](https://www.linuxmint.com/)),
the packages can be installed with `apt-get` or `apt` as follows:

```
sudo apt-get install autoconf autoconf-archive autopoint automake \
  cmake make dbus doxygen graphviz g++ gettext libasound2-dev \
  libavcodec-dev libavdevice-dev libavformat-dev libboost-dev \
  libcppunit-dev libdbus-1-dev libdbus-c++-dev libexpat1-dev \
  libgnutls28-dev libgtk-3-dev libjack-dev libopus-dev \
  libpcre3-dev libpulse-dev libssl-dev libspeex-dev \
  libspeexdsp-dev libswscale-dev libtool libudev-dev \
  libyaml-cpp-dev sip-tester swig uuid-dev yasm libjsoncpp-dev \
  libva-dev libvdpau-dev libmsgpack-dev pandoc nasm dpkg-dev
```

On RPM-based GNU/Linux distributions
(such as [Fedora](https://fedoraproject.org/) and [openSUSE](https://www.opensuse.org/)),
the packages can be installed with `dnf`, `yum`, or `zypper` as follows:

```
sudo dnf install autoconf autoconf-archive automake cmake make \
  speexdsp-devel pulseaudio-libs-devel libtool dbus-devel \
  expat-devel pcre-devel doxygen graphviz yaml-cpp-devel \
  boost-devel dbus-c++-devel dbus-devel libXext-devel \
  libXfixes-devel yasm speex-devel chrpath check uuid-c++-devel \
  gettext-devel gcc-c++ which alsa-lib-devel systemd-devel \
  libuuid-devel uuid-devel gnutls-devel nettle-devel opus-devel \
  speexdsp-devel yaml-cpp-devel swig jsoncpp-devel patch \
  libva-devel openssl-devel libvdpau-devel msgpack-devel \
  sqlite-devel openssl-static pandoc nasm bzip2
```

If there are any issues with missing dependencies, please check:
* The Jami package definitions, distributed on [dl.jami.net](https://dl.jami.net/); and
* The `build.py` script, on the `jami-project` repository.

## jami-client-qt

```{seealso}
For complete instructions on how to build the Qt Desktop client,
please refer to the [INSTALL.md](https://git.jami.net/savoirfairelinux/jami-client-qt/-/blob/master/INSTALL.md) file.
```

Building the Jami client requires the following tools and libraries:

- jami-daemon
- libnm
- libnotify
- libqrencode
- libayatana-appindicator or libappindicator (optional, for notifications)
- qt6-base
- qt6-declarative
- qt6-graphicaleffects
- qt6-multimedia
- qt6-networkauth
- qt6-svg
- qt6-tools
- qt6-webengine (optional, currently for link previews and some media
  file previews)

The libqt-jami and jami-libqt packages from [dl.jami.net](https://dl.jami.net/) provide the required Qt 6 dependencies on supported dpkg/Debian-based and RPM-based GNU/Linux distributions, respectively.
This is especially useful for building Jami on slightly older versions of these distributions where Qt 6 is not yet packaged in the distribution's official repositories.
To install this package providing the Qt 6 dependencies on a supported distribution:
1. Add the respective [dl.jami.net](https://dl.jami.net/) repository by following the instructions on the [Download Jami for GNU/Linux](https://jami.net/download-jami-linux) webpage; and
2. Install the package using the Terminal command `sudo apt-get install libqt-jami` (for dpkg/Debian-based GNU/Linux distributions) or `sudo dnf install jami-libqt` (for RPM-based GNU/Linux distributions).

On dpkg/Debian-based GNU/Linux distributions
(such as Debian, Trisquel, Ubuntu, and Linux Mint),
the packages can be installed with `apt-get` or `apt` as follows:

```
sudo apt-get install qt6-base-dev qt6-tools-dev \
  qt6-tools-dev-tools qt6-l10n-tools libqt6sql6-sqlite \
  libqt6core5compat6-dev libqt6networkauth6-dev \
  qt6-multimedia-dev libqt6svg6-dev qt6-declarative-dev \
  qml6-module-qt-labs-qmlmodels \
  qml6-module-qt5compat-graphicaleffects \
  qml6-module-qtqml-workerscript qml6-module-qtmultimedia \
  qml6-module-qtquick qml6-module-qtquick-controls \
  qml6-module-qtquick-dialogs qml6-module-qtquick-layouts \
  qml6-module-qtquick-shapes qml6-module-qtquick-window \
  qml6-module-qtquick-templates qml6-module-qt-labs-platform \
  libqrencode-dev libnm-dev libnotify-dev
```

To install the optional Qt WebEngine packages:

```
sudo apt-get install libqt6webengine6-data \
  libqt6webenginecore6-bin qt6-webengine-dev \
  qt6-webengine-dev-tools qml6-module-qtwebengine \
  qml6-module-qtwebchannel
```

On RPM-based GNU/Linux distributions
(such as Fedora and openSUSE),
the packages can be installed with `dnf`, `yum`, or `zypper` as follows:

```
sudo dnf install qt6-qtbase-devel qt6-qtsvg-devel \
  qt6-qtmultimedia-devel qt6-qtdeclarative-devel qrencode-devel \
  NetworkManager-libnm-devel
```

To install the optional Qt WebEngine packages:

```
sudo dnf install qt6-qtwebengine-devel
```

```{note}
The easiest approach would be to install the libqt-jami or jami-libqt package on your supported dpkg/Debian-based or RPM-based GNU/Linux distribution, respectively.
This would ensure that:
* All the required Qt 6 dependencies are fetched; and
* The application is as similar as possible at what is being built and tested.
```