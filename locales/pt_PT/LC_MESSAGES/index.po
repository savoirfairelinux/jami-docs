# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2025 Savoir-faire Linux Inc. and contributors
# This file is distributed under the same license as the Jami package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# Rui <xymarior@yandex.com>, 2022
# savoirfairelinux <support@savoirfairelinux.com>, 2023
# lecalam, 2025
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Jami\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 03:53+0000\n"
"PO-Revision-Date: 2022-09-14 17:41+0000\n"
"Last-Translator: lecalam, 2025\n"
"Language-Team: Portuguese (Portugal) (https://app.transifex.com/savoirfairelinux/teams/49466/pt_PT/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: pt_PT\n"
"Plural-Forms: nplurals=3; plural=(n == 0 || n == 1) ? 0 : n != 0 && n % 1000000 == 0 ? 1 : 2;\n"

#: ../../../index.md:1
msgid "Home"
msgstr "Início"

#: ../../../index.md:3
msgid "![«Image: Jami logo»](_static/logo-jami.svg \"Jami logo\")"
msgstr "![“Imagem: logótipo Jami”](_static/logo-jami.svg “logótipo Jami”)"

#: ../../../index.md:3
msgid "«Image: Jami logo»"
msgstr "«Imagem: logótipo Jami»"

#: ../../../index.md:5
msgid ""
"This is the documentation for [Jami](https://jami.net), free/libre software "
"for [universal communication](user/introduction.md) that respects the "
"freedom and privacy of its users. Jami is available across operating systems"
" and platforms including GNU/Linux, macOS, Windows, Android/Replicant, and "
"iOS."
msgstr ""
"Esta é a documentação do [Jami](https://jami.net), software livre para "
"[comunicação universal](user/introduction.md) que respeita a liberdade e a "
"privacidade dos seus utilizadores. O Jami está disponível em todos os "
"sistemas operativos e plataformas, incluindo GNU/Linux, macOS, Windows, "
"Android/Replicante e iOS."

#: ../../../index.md:8
msgid ""
"Jami is an official GNU package and you can redistribute it and/or modify it"
" under the terms of the [GNU General Public "
"License](https://www.gnu.org/licenses/gpl.html) as published by the Free "
"Software Foundation, either version 3 of the License, or (at your option) "
"any later version. The Jami team [uses Gerrit for "
"development](developer/new-developers/working-with-gerrit.md) and code "
"reviews, and issues and feature requests are [tracked on the Jami "
"GitLab](user/bug-report-guide.md). The development of Jami is led by "
"[Savoir-faire Linux Inc.](https://savoirfairelinux.com/en) --- a Canadian "
"GNU/Linux consulting company based in Montréal, Québec --- and is supported "
"by a growing global community of users and contributors."
msgstr ""
"Jami é um pacote GNU oficial e você pode redistribuí-lo e/ou modificá-lo sob"
" os termos da [GNU General Public "
"License](https://www.gnu.org/licenses/gpl.html) como publicada pela Free "
"Software Foundation, seja a versão 3 da Licença, ou (a seu critério) "
"qualquer versão posterior. A equipa do Jami [utiliza o Gerrit para o "
"desenvolvimento](developer/new-developers/working-with-gerrit.md) e revisões"
" de código, e os problemas e pedidos de funcionalidades são [seguidos no "
"Jami GitLab](user/bug-report-guide.md). O desenvolvimento do Jami é liderado"
" pela [Savoir-faire Linux Inc.](https://savoirfairelinux.com/en) --- uma "
"empresa canadiana de consultoria GNU/Linux sediada em Montréal, Québec --- e"
" é apoiado por uma crescente comunidade global de utilizadores e "
"contribuidores."

#: ../../../index.md:12
msgid ""
"This documentation is community-driven and [anyone can "
"contribute](contribute.md#contributing-to-this-documentation)!"
msgstr ""
"Esta documentação é orientada para a comunidade e [qualquer pessoa pode "
"contribuir](contribute.md#contributing-to-this-documentation)!"

#: ../../../index.md:15
msgid ""
"You may also be interested in the daemon's [code "
"coverage](https://docs.jami.net/coverage/), or the [OpenDHT "
"wiki](https://github.com/savoirfairelinux/opendht/wiki)."
msgstr ""
"Também pode estar interessado na [cobertura de código] do daemon "
"(https://docs.jami.net/coverage/) ou no [OpenDHT "
"wiki](https://github.com/savoirfairelinux/opendht/wiki)."

#: ../../../index.md:19
msgid "Documentation manuals"
msgstr "Manuais de documentação"

#: ../../../index.md:21
msgid ""
"Here are the documentation manuals of Jami and details on how to contribute "
"to Jami:"
msgstr ""
"Aqui estão os manuais de documentação do Jami e detalhes sobre como "
"contribuir para o Jami:"

#: ../../../index.md:35
msgid "Community and support"
msgstr "Comunidade e apoio"

#: ../../../index.md:37
msgid "Learn more about:"
msgstr "Saiba mais sobre:"

#: ../../../index.md:39
msgid "Jami: <https://jami.net/>"
msgstr "Jami: <https://jami.net/>"

#: ../../../index.md:40
msgid "Jami extensions: <https://jami.net/extensions/>"
msgstr "Extensões do Jami: <https://jami.net/extensions/>"

#: ../../../index.md:41
msgid "“Jami Account Management Server” (JAMS): <https://jami.biz/>"
msgstr "“Jami Account Management Server” (JAMS): <https://jami.biz/>"

#: ../../../index.md:42
msgid "Jami documentation: <https://docs.jami.net/>"
msgstr "Documentação Jami: <https://docs.jami.net/>"

#: ../../../index.md:44
msgid "Follow us for more:"
msgstr "Siga-nos para saber mais:"

#: ../../../index.md:46
msgid "Mastodon: [@Jami@mstdn.io](https://mstdn.io/@Jami)"
msgstr "Mastodon: [@Jami@mstdn.io](https://mstdn.io/@Jami)"

#: ../../../index.md:47
msgid "X: [@jami_social](https://x.com/jami_social)"
msgstr "X: [@jami_social](https://x.com/jami_social)"

#: ../../../index.md:48
msgid "YouTube: [@jami9311](https://www.youtube.com/@jami9311)"
msgstr "YouTube: [@jami9311](https://www.youtube.com/@jami9311)"

#: ../../../index.md:50
msgid "We’d love to hear from you! Join the Jami community:"
msgstr "Gostaríamos de o ouvir! Junte-se à comunidade Jami:"

#: ../../../index.md:52
msgid "Contribute: <https://jami.net/contribute/>"
msgstr "Contribuir: <https://jami.net/contribute/>"

#: ../../../index.md:53
msgid "Forum: <https://forum.jami.net/>"
msgstr "Fórum: <https://forum.jami.net/>"

#: ../../../index.md:54
msgid ""
"GNU Mailman: [jami@gnu.org](https://lists.gnu.org/mailman/listinfo/jami)"
msgstr ""
"GNU Mailman: [jami@gnu.org](https://lists.gnu.org/mailman/listinfo/jami)"

#: ../../../index.md:55
msgid "Libera.Chat: [#jami](https://web.libera.chat/#jami)"
msgstr "Libera.Chat: [#jami](https://web.libera.chat/#jami)"

#: ../../../index.md:56
msgid ""
"Matrix: [#jami:matrix.org](https://matrix.to/#/#jami:matrix.org) (bridged "
"with Libera.Chat)"
msgstr ""
"Matriz: [#jami:matrix.org](https://matrix.to/#/#jami:matrix.org) (em ponte "
"com Libera.Chat)"

#: ../../../index.md:59
msgid ""
"You can also contact the Jami team at Savoir-faire Linux Inc. directly via "
"email at <contact@jami.net>.  Additionally, Savoir-faire Linux Inc. offers "
"commercial support for Jami via <https://jami.biz>."
msgstr ""
"Também pode contactar a equipa Jami na Savoir-faire Linux Inc. diretamente "
"através do e-mail <contact@jami.net>.  Além disso, a Savoir-faire Linux Inc."
" oferece suporte comercial para o Jami via <https://jami.biz>."
