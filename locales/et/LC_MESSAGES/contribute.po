# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2025 Savoir-faire Linux Inc. and contributors
# This file is distributed under the same license as the Jami package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# savoirfairelinux <support@savoirfairelinux.com>, 2023
# Priit Jõerüüt <transifex@joeruut.com>, 2025
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Jami\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 03:53+0000\n"
"PO-Revision-Date: 2022-09-14 17:52+0000\n"
"Last-Translator: Priit Jõerüüt <transifex@joeruut.com>, 2025\n"
"Language-Team: Estonian (https://app.transifex.com/savoirfairelinux/teams/49466/et/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: et\n"
"Plural-Forms: nplurals=2; plural=(n != 1);\n"

#: ../../../contribute.md:1
msgid "Contribute"
msgstr "Osale kaastöös"

#: ../../../contribute.md:3
msgid ""
"Contributions to Jami are always welcome and are much appreciated. There are"
" many ways to contribute to Jami, including:"
msgstr ""

#: ../../../contribute.md:5
msgid "Reporting bugs and issues,"
msgstr ""

#: ../../../contribute.md:6
msgid "Contributing code,"
msgstr ""

#: ../../../contribute.md:7
msgid ""
"Helping package and maintain Jami for your GNU/Linux distribution or other "
"operating system,"
msgstr ""

#: ../../../contribute.md:8
msgid "Contributing to the [Jami documentation](https://docs.jami.net/)."
msgstr ""

#: ../../../contribute.md:10
msgid "Please see below for how to get started contributing to Jami!"
msgstr "Palun vaata allpool, kuidas alustada Jami toetust!"

#: ../../../contribute.md:12
msgid "Reporting bugs and issues"
msgstr "Raporteerimine vigade ja probleemide kohta"

#: ../../../contribute.md:14
msgid ""
"Please see the [Bug report guide](user/bug-report-guide.md) for step-by-step"
" instructions on how to report any issue you may encounter with Jami."
msgstr ""

#: ../../../contribute.md:16
msgid "Contributing code"
msgstr "Toetustöö kood"

#: ../../../contribute.md:18
msgid ""
"To start contributing to Jami, look at the good first issues at: <a "
"href=\"https://git.jami.net/groups/savoirfairelinux/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good%20first%20issue\">"
" "
"https://git.jami.net/groups/savoirfairelinux/-/issues/?sort=created_date&state=opened&label_name[]=good"
" first issue </a>."
msgstr ""

#: ../../../contribute.md:23
msgid ""
"Contact the developers directly by adding a comment on the ticket. This will"
" enable the developers to guide you through the process."
msgstr ""

#: ../../../contribute.md:26
msgid ""
"A push with a patch to <https://review.jami.net> will be required to "
"integrate the code to Jami."
msgstr ""

#: ../../../contribute.md:29
msgid ""
"For more information on how to push a patch, please refer to the [Working "
"with Gerrit](developer/new-developers/working-with-gerrit.md) guide."
msgstr ""

#: ../../../contribute.md:32
msgid "Commit message guidelines"
msgstr ""

#: ../../../contribute.md:34
msgid ""
"When submitting a patch to Jami, please follow the following guidelines for "
"the commit messages:"
msgstr ""

#: ../../../contribute.md:36
msgid ""
"The first line should include the component or scope of the change, followed"
" by a short summary of the change in the imperative mood (e.g., \"add new "
"function\", \"fix bug\", \"update documentation\")."
msgstr ""

#: ../../../contribute.md:38
msgid ""
"The subject can use the capitalization of the component or scope, but the "
"rest of the title should be lowercase."
msgstr ""

#: ../../../contribute.md:40
msgid "The second line should be blank."
msgstr ""

#: ../../../contribute.md:42
msgid ""
"The third line should be the beginning of a longer description of the change"
" in complete sentences, if necessary."
msgstr ""

#: ../../../contribute.md:44
msgid ""
"50/72 rule: The first line should be no longer than 50 characters (ideally),"
" and the rest of the message should be wrapped at 72 characters per line. "
"This can be configured in your text editor."
msgstr ""

#: ../../../contribute.md:47
msgid ""
"If the change is related to a specific issue in the Jami GitLab, include the"
" issue number in the commit message. For example: `GitLab: #123`. If the "
"change is related to multiple issues, list them all. If the change is "
"related to an issue that is not part of the project, use a link to the issue"
" instead."
msgstr ""

#: ../../../contribute.md:52
msgid "Template for a commit message:"
msgstr ""

#: ../../../contribute.md:66
msgid "For example:"
msgstr "Näiteks:"

#: ../../../contribute.md:79
msgid "Packaging Jami"
msgstr "Pakendamine Jami"

#: ../../../contribute.md:81
msgid "There are two possible ways to package Jami:"
msgstr ""

#: ../../../contribute.md:82
msgid ""
"Via our internal process to create packages available on the Snap Store or "
"<https://dl.jami.net>."
msgstr ""

#: ../../../contribute.md:83
msgid "Via the packaging process of your favorite GNU/Linux distribution."
msgstr ""

#: ../../../contribute.md:86
msgid ""
"Jami is a quite complex project with a lot of dependencies. This isn't a "
"quick and easy task and requires maintenance."
msgstr ""

#: ../../../contribute.md:91
msgid "If Jami is packaged using the second option:"
msgstr ""

#: ../../../contribute.md:92
msgid ""
"For official releases, Jami uses certain [Qt](https://www.qt.io/) versions "
"because Qt is a big dependency. This is to ensure that Jami works with the "
"version we use. Every slight change in the Qt version can break Jami or "
"bring some small unwanted changes. For example, 6.2 → 6.4 broke the video "
"pipeline."
msgstr ""

#: ../../../contribute.md:96
msgid ""
"Jami uses a fork of the [pjproject](https://github.com/pjsip/pjproject), as "
"ICE over TCP is a requirement that is not planned upstream."
msgstr ""

#: ../../../contribute.md:97
msgid ""
"[libupnp](https://pupnp.sourceforge.io/) received some patches to ensure "
"that it's built with the non-blocking API."
msgstr ""

#: ../../../contribute.md:98
msgid "[FFmpeg](https://ffmpeg.org/) has some screen sharing patches."
msgstr ""

#: ../../../contribute.md:99
msgid "Visit `daemon/contrib/src` to check how dependencies are built."
msgstr ""

#: ../../../contribute.md:102
msgid ""
"For internal packaging, everything is located in `extras/packaging/gnu-"
"linux`. You can follow previous patches to understand your needs. e.g., "
"<https://review.jami.net/c/jami-client-qt/+/28036>"
msgstr ""

#: ../../../contribute.md:105
msgid "We use 3 channels:"
msgstr ""

#: ../../../contribute.md:106
msgid "Internal for testing purposes"
msgstr ""

#: ../../../contribute.md:107
msgid "Nightly/Beta/Edge for public beta"
msgstr ""

#: ../../../contribute.md:108
msgid "Stable for public releases"
msgstr ""

#: ../../../contribute.md:110
msgid ""
"We generally use internal when we test new distributions or when we package "
"a new Qt version. Then we try to generate a nightly per week and one stable "
"per month (if unit tests are green)."
msgstr ""

#: ../../../contribute.md:113
msgid "Packages are pushed to:"
msgstr ""

#: ../../../contribute.md:114
msgid ""
"[dl.jami.net](https://dl.jami.net/) (2 machines, with an rsync every 15 min)"
msgstr ""

#: ../../../contribute.md:115
msgid "Ubuntu store ([snapcraft.io/jami](https://snapcraft.io/jami))"
msgstr ""

#: ../../../contribute.md:117
msgid "If you want to add a new distribution, you will need to:"
msgstr ""

#: ../../../contribute.md:118
msgid "Add Dockerfile"
msgstr ""

#: ../../../contribute.md:119
msgid "Change Makefile"
msgstr ""

#: ../../../contribute.md:120
msgid "Update packaging script"
msgstr ""

#: ../../../contribute.md:121
msgid "Cross fingers"
msgstr ""

#: ../../../contribute.md:122
msgid "Test the package in a VM"
msgstr ""

#: ../../../contribute.md:125
msgid ""
"Chromium is a hard part to build. Three common problems encountered are:"
msgstr ""

#: ../../../contribute.md:127
msgid "GCC is too recent:"
msgstr ""

#: ../../../contribute.md:128
msgid ""
"Generally the fix consist of importing patches from Chromium's gerrit to fix"
" GCC issues"
msgstr ""

#: ../../../contribute.md:129
msgid "Python is too recent:"
msgstr ""

#: ../../../contribute.md:130
msgid ""
"Generally the fix consist of using PyEnv to get a virtual environment with "
"the correct Python's version"
msgstr ""

#: ../../../contribute.md:131
msgid "Missing dependencies:"
msgstr ""

#: ../../../contribute.md:132
msgid ""
"During the configuration step of Qt, the list of built components is listed "
"and the missing dependencies are listed. Generally installing a package or "
"updating node.js fix the issue"
msgstr ""

#: ../../../contribute.md:133
msgid ""
"Note that if Qt is generated without chromium, we must remove the package in"
" the cache of the build machines to regenerate a new one (/var/cache/jami)"
msgstr ""

#: ../../../contribute.md:136
msgid "If you want to remove a distribution:"
msgstr ""

#: ../../../contribute.md:137
msgid ""
"If a distribution is EOL OR if there are 2 more recent LTS, we can remove "
"the distribution (e.g., Ubuntu 20, 22, 24—remove Ubuntu 20) by removing "
"related files and checks."
msgstr ""

#: ../../../contribute.md:140
msgid "The next big changes required are:"
msgstr ""

#: ../../../contribute.md:141
msgid "Use [CMake](https://cmake.org/) instead of autotools for jami-daemon."
msgstr ""

#: ../../../contribute.md:142
msgid ""
"Use [Ubuntu Core 22 (UC22)](https://ubuntu.com/core/docs/uc22) instead of "
"core20 in snap."
msgstr ""

#: ../../../contribute.md:143
msgid ""
"[Flatpak](https://flatpak.org/)/[AppImage](https://appimage.org/) support? "
"This may simplify custom RPM/Debian packaging."
msgstr ""

#: ../../../contribute.md:144
msgid "Only generate one unified Debian file per distribution. Same for RPMs."
msgstr ""

#: ../../../contribute.md:145
msgid ""
"Use [Jenkinsfile](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/) to "
"generate packages for GNU/Linux, macOS, and Windows at the same time if "
"possible."
msgstr ""

#: ../../../contribute.md:148
msgid ""
"For internal information (such as how to publish to stores, cf. internal "
"wiki)."
msgstr ""

#: ../../../contribute.md:150
msgid "Contributing to this documentation"
msgstr "Selle dokumentatsiooni koostamisel"

#: ../../../contribute.md:152
msgid ""
"Contributions to these docs are always welcome and appreciated, from small "
"corrections to whole new chapters."
msgstr ""
"Nende dokumentide panused on alati teretulnud ja hinnatud, alates väikestest"
" parandustest kuni täiesti uuest peatükist."

#: ../../../contribute.md:154
msgid ""
"This page will walk through the steps to create a new page or submit a "
"correction. The patch review process is the same as for any other Jami "
"project, so we will not explain every command."
msgstr ""

#: ../../../contribute.md:158
msgid ""
"By contributing to this documentation, you agree to make your contributions "
"available under the [fdl](fdl.md), Version 1.3 or any later version "
"published by the Free Software Foundation; with no Invariant Sections, no "
"Front-Cover Texts, and no Back-Cover Texts."
msgstr ""

#: ../../../contribute.md:160
msgid ""
"You are also promising that you are the author of your changes, or that you "
"copied them from a work in the public domain or a work released under a free"
" license that is compatible with the [fdl](fdl.md). DO NOT SUBMIT "
"COPYRIGHTED WORK WITHOUT PERMISSION."
msgstr ""

#: ../../../contribute.md:165
msgid ""
"If you want to help to translate this page, you can join the project and "
"start translating this page on "
"<https://explore.transifex.com/savoirfairelinux/>."
msgstr ""

#: ../../../contribute.md:169
msgid "Dependencies"
msgstr "Sõltumised"

#: ../../../contribute.md:171
msgid ""
"Git is required to be installed and configured to use your SSH keypair and "
"an account on the [Jami Gerrit](https://review.jami.net), where you would "
"send your patches for review. If you need help with this, see the beginning "
"of our patch submission guide (TODO)."
msgstr ""

#: ../../../contribute.md:174
msgid ""
"If you want to preview your changes locally in your web browser, you need to"
" install:"
msgstr ""

#: ../../../contribute.md:175
msgid "[Sphinx](https://www.sphinx-doc.org),"
msgstr ""

#: ../../../contribute.md:176
msgid ""
"[Read the Docs Sphinx theme](https://sphinx-rtd-"
"theme.readthedocs.io/en/stable/), and"
msgstr ""

#: ../../../contribute.md:177
msgid ""
"[MyST Markdown parser](https://myst-"
"parser.readthedocs.io/en/latest/index.html)."
msgstr ""

#: ../../../contribute.md:183
msgid ""
"If you want to use the auto-build and auto-refresh feature, also install "
"[sphinx-autobuild](https://github.com/executablebooks/sphinx-autobuild)."
msgstr ""

#: ../../../contribute.md:190
msgid "Cloning the repository"
msgstr "Kloonimine ladustuses"

#: ../../../contribute.md:192
msgid "Clone the repository and configure the push settings like this:"
msgstr "Klooneerima lauda ja konfigureerida push seadistused nii:"

#: ../../../contribute.md:200
msgid ""
"You may want to check out a new branch for each contribution/change before "
"you make any change to the files so that you could easily `git pull` any "
"future changes from upstream into your main local branch:"
msgstr ""

#: ../../../contribute.md:206
msgid "Editing a page"
msgstr "Lehekülje redigeerimine"

#: ../../../contribute.md:208
msgid ""
"Pages are written in [Markdown](https://www.markdownguide.org/). Click "
"\"View page source\" at the top of any page to open the raw source of the "
"page and see how it was written."
msgstr ""

#: ../../../contribute.md:211
msgid "Go ahead and make your changes to the `.md` files."
msgstr ""

#: ../../../contribute.md:213
msgid "Previewing your work"
msgstr "Teie töö läbivaatamine"

#: ../../../contribute.md:215
msgid "From the base of the repository, run:"
msgstr "Repositoriumi alust käivitada:"

#: ../../../contribute.md:221
msgid ""
"You should now be able to view the documentation in your web browser. The "
"homepage is at `_build/html/index.html`."
msgstr ""

#: ../../../contribute.md:225
msgid ""
"This documentation does not currently build with the latest version of "
"[Sphinx](https://www.sphinx-doc.org/). Please see [this issue on "
"GitLab](https://git.jami.net/savoirfairelinux/jami-docs/-/issues/15) for a "
"workaround and updates regarding this problem."
msgstr ""

#: ../../../contribute.md:230
msgid ""
"To automatically build the documentation and refresh your web browser "
"whenever you save changes, run:"
msgstr ""
"Dokumendid automaatselt koostamiseks ja veebilehtirühmade värskendamiseks, "
"kui salvestate muudatusi, käivitage:"

#: ../../../contribute.md:236
msgid ""
"Keep this running in the background, then navigate to "
"`http://127.0.0.1:8000` (*not* the local .html file)."
msgstr ""

#: ../../../contribute.md:238
msgid "Saving your work"
msgstr "Tööde säästmine"

#: ../../../contribute.md:245
msgid ""
"Refer to the [commit message guidelines](contribute.md#commit-message-"
"guidelines) for how to write a good commit message."
msgstr ""

#: ../../../contribute.md:247
msgid "Submitting a change"
msgstr "Muudatus esitamine"

#: ../../../contribute.md:249
msgid ""
"The first time you try to push your changes, Gerrit will complain that you "
"don't have a Change-Id in your commit, and provide an `scp` command to "
"install the commit hook. After running the command, you should be able to "
"recommit and push your change:"
msgstr ""

#: ../../../contribute.md:257
msgid "Modifying your work"
msgstr "Muudates oma tööd"

#: ../../../contribute.md:259
msgid ""
"A reviewer may ask you to make changes to your patch before merging it. This"
" is no problem! Simply make the changes, `git add` them, and run `git commit"
" --amend` to modify the patch."
msgstr ""

#: ../../../contribute.md:264
msgid ""
"The `--amend` switch, which is required to tell Git to *amend*/tweak the "
"existing newest commit rather than making a new commit. This is the workflow"
" for updating a proposed change when using Gerrit."
msgstr ""

#: ../../../contribute.md:268
msgid "Adding a page"
msgstr "Lehekülje lisamine"

#: ../../../contribute.md:270
msgid ""
"If you decide to add a whole new page to the documentation, you must also "
"add it to the `toctree` directive of that chapter."
msgstr ""

#: ../../../contribute.md:272
msgid ""
"For instance, if you added a new page called `hosting-jams-on-aws-guide.md` "
"to the Jami user manual in the `user` folder, you should add it in the "
"`toctree` directive of `user/index.md`, *without* the file extension:"
msgstr ""
