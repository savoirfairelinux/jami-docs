# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2018-2025 Savoir-faire Linux Inc. and contributors
# This file is distributed under the same license as the Jami package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
# 
# Translators:
# savoirfairelinux <support@savoirfairelinux.com>, 2023
# 
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: Jami\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2025-02-04 03:53+0000\n"
"PO-Revision-Date: 2022-09-14 17:46+0000\n"
"Last-Translator: savoirfairelinux <support@savoirfairelinux.com>, 2023\n"
"Language-Team: Persian (https://app.transifex.com/savoirfairelinux/teams/49466/fa/)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Language: fa\n"
"Plural-Forms: nplurals=2; plural=(n > 1);\n"

#: ../../../build/dependencies.md:1
msgid "Dependencies"
msgstr "وابستگی ها"

#: ../../../build/dependencies.md:3
msgid ""
"Jami is a large and complex piece of software, and depends on several "
"external free software libraries. The dependencies and how to install them "
"on some systems are outlined below."
msgstr ""

#: ../../../build/dependencies.md:9 ../../../build/dependencies.md:104
msgid "jami-daemon"
msgstr "جامی-دائون"

#: ../../../build/dependencies.md:11
msgid "Building the Jami daemon requires the following tools and libraries:"
msgstr ""

#: ../../../build/dependencies.md:13
msgid "GNU Autotools (autoconf, autoconf-archive, autopoint, and automake)"
msgstr "GNU Autotools (autoconf، آٹوکانف آرکائیو، آٹوپونت و اتوماتیک)"

#: ../../../build/dependencies.md:14
msgid "GCC (GNU Compiler Collection) C++ compiler (g++)"
msgstr "GCC (GNU Compiler Collection) C++ (g++)"

#: ../../../build/dependencies.md:15
msgid "[GNU Make](https://www.gnu.org/software/make/manual/)"
msgstr ""

#: ../../../build/dependencies.md:16
msgid "[GNU gettext](https://www.gnu.org/software/gettext/)"
msgstr ""

#: ../../../build/dependencies.md:17
msgid "[GNU Libtool](https://www.gnu.org/software/libtool/)"
msgstr ""

#: ../../../build/dependencies.md:18
msgid "[GnuTLS](https://gnutls.org/)"
msgstr ""

#: ../../../build/dependencies.md:19
msgid "[Argon2](https://en.wikipedia.org/wiki/Argon2)"
msgstr ""

#: ../../../build/dependencies.md:20
msgid "[asio](https://en.wikipedia.org/wiki/Audio_Stream_Input/Output)"
msgstr ""

#: ../../../build/dependencies.md:21
msgid "[dbus](https://www.freedesktop.org/wiki/Software/dbus/)"
msgstr ""

#: ../../../build/dependencies.md:22
msgid "dbus-c++"
msgstr "dbus-c++"

#: ../../../build/dependencies.md:23
msgid "[libupnp](https://github.com/pupnp/pupnp)"
msgstr ""

#: ../../../build/dependencies.md:24
msgid "[libssl](https://www.openssl.org/)"
msgstr ""

#: ../../../build/dependencies.md:25
msgid "[libpulse](https://gitlab.freedesktop.org/pulseaudio/pulseaudio)"
msgstr ""

#: ../../../build/dependencies.md:26
msgid "[libasound2](https://github.com/pop-os/libasound2)"
msgstr ""

#: ../../../build/dependencies.md:27
msgid "[libexpat](https://libexpat.github.io/)"
msgstr ""

#: ../../../build/dependencies.md:28
msgid "[pcre3](https://www.pcre.org/)"
msgstr ""

#: ../../../build/dependencies.md:29
msgid "[yaml-cpp](https://github.com/jbeder/yaml-cpp)"
msgstr ""

#: ../../../build/dependencies.md:30
msgid "[libXext](https://gitlab.freedesktop.org/xorg/lib/libxext)"
msgstr ""

#: ../../../build/dependencies.md:31
msgid "libxfixes"
msgstr "اصلاحات"

#: ../../../build/dependencies.md:32
msgid "[speex](https://www.speex.org/)"
msgstr ""

#: ../../../build/dependencies.md:33
msgid "speexdsp"
msgstr "اسپیکس"

#: ../../../build/dependencies.md:34
msgid "uuid"
msgstr "آید"

#: ../../../build/dependencies.md:35
msgid ""
"[FFmpeg](https://ffmpeg.org/) (libavcodec, libavdevice, libswscale, and "
"libavutil)"
msgstr ""

#: ../../../build/dependencies.md:36
msgid "[opus](https://opus-codec.org/)"
msgstr ""

#: ../../../build/dependencies.md:37
msgid "[udev](https://en.wikipedia.org/wiki/Udev)"
msgstr ""

#: ../../../build/dependencies.md:38
msgid "[fmt](https://github.com/fmtlib/fmt)"
msgstr ""

#: ../../../build/dependencies.md:39
msgid "gsm"
msgstr "GSM"

#: ../../../build/dependencies.md:40
msgid "[pjproject](https://www.pjsip.org/) (custom Jami fork required)"
msgstr ""

#: ../../../build/dependencies.md:41
msgid "[jsoncpp](https://github.com/open-source-parsers/jsoncpp)"
msgstr ""

#: ../../../build/dependencies.md:42
msgid "[libarchive](https://libarchive.org/)"
msgstr ""

#: ../../../build/dependencies.md:43
msgid "[libnatpmp](https://github.com/miniupnp/libnatpmp)"
msgstr ""

#: ../../../build/dependencies.md:44
msgid "[libva](https://github.com/intel/libva)"
msgstr ""

#: ../../../build/dependencies.md:45
msgid "[libcrypto++](https://github.com/weidai11/cryptopp)"
msgstr ""

#: ../../../build/dependencies.md:46
msgid "[libvdpau](https://en.wikipedia.org/wiki/VDPAU)"
msgstr ""

#: ../../../build/dependencies.md:47
msgid "[libsecp256k1](https://github.com/bitcoin-core/secp256k1)"
msgstr ""

#: ../../../build/dependencies.md:48
msgid "[libgit2](https://libgit2.org/)"
msgstr ""

#: ../../../build/dependencies.md:49
msgid "[http-parser](https://pub.dev/packages/http_parser)"
msgstr ""

#: ../../../build/dependencies.md:50
msgid "[nasm](https://www.nasm.us/)"
msgstr ""

#: ../../../build/dependencies.md:51
msgid "[yasm](https://yasm.tortall.net/)"
msgstr ""

#: ../../../build/dependencies.md:52
msgid "[Nettle](https://www.lysator.liu.se/~nisse/nettle/)"
msgstr ""

#: ../../../build/dependencies.md:53
msgid "[OpenDHT](https://github.com/savoirfairelinux/opendht)"
msgstr ""

#: ../../../build/dependencies.md:54
msgid "[RESTinio](https://github.com/Stiffstream/restinio)"
msgstr ""

#: ../../../build/dependencies.md:55
msgid ""
"[webrtc-audio-processing](https://github.com/tonarino/webrtc-audio-"
"processing)"
msgstr ""

#: ../../../build/dependencies.md:56
msgid "[zlib](https://zlib.net/)"
msgstr ""

#: ../../../build/dependencies.md:58
msgid ""
"On dpkg/Debian-based GNU/Linux distributions (such as "
"[Debian](https://www.debian.org/), [Trisquel](https://trisquel.info/), "
"[Ubuntu](https://ubuntu.com/), and [Linux "
"Mint](https://www.linuxmint.com/)), the packages can be installed with `apt-"
"get` or `apt` as follows:"
msgstr ""

#: ../../../build/dependencies.md:74
msgid ""
"On RPM-based GNU/Linux distributions (such as "
"[Fedora](https://fedoraproject.org/) and "
"[openSUSE](https://www.opensuse.org/)), the packages can be installed with "
"`dnf`, `yum`, or `zypper` as follows:"
msgstr ""

#: ../../../build/dependencies.md:91
msgid "If there are any issues with missing dependencies, please check:"
msgstr ""

#: ../../../build/dependencies.md:92
msgid ""
"The Jami package definitions, distributed on "
"[dl.jami.net](https://dl.jami.net/); and"
msgstr ""

#: ../../../build/dependencies.md:93
msgid "The `build.py` script, on the `jami-project` repository."
msgstr ""

#: ../../../build/dependencies.md:95
msgid "jami-client-qt"
msgstr "jami-client-qt"

#: ../../../build/dependencies.md:98
msgid ""
"For complete instructions on how to build the Qt Desktop client, please "
"refer to the [INSTALL.md](https://git.jami.net/savoirfairelinux/jami-client-"
"qt/-/blob/master/INSTALL.md) file."
msgstr ""

#: ../../../build/dependencies.md:102
msgid "Building the Jami client requires the following tools and libraries:"
msgstr ""

#: ../../../build/dependencies.md:105
msgid "libnm"
msgstr "لیب نیم"

#: ../../../build/dependencies.md:106
msgid "libnotify"
msgstr "اطلاع رسانی"

#: ../../../build/dependencies.md:107
msgid "libqrencode"
msgstr "کد libqren"

#: ../../../build/dependencies.md:108
msgid ""
"libayatana-appindicator or libappindicator (optional, for notifications)"
msgstr "شاخص اعمال لیبیانا یا لیباپینڈیکاتور (فروخته، برای اطلاعیه ها)"

#: ../../../build/dependencies.md:109
msgid "qt6-base"
msgstr "قتیل6"

#: ../../../build/dependencies.md:110
msgid "qt6-declarative"
msgstr "اعلامیه qt6"

#: ../../../build/dependencies.md:111
msgid "qt6-graphicaleffects"
msgstr "Qt6- اثرات گرافیکی"

#: ../../../build/dependencies.md:112
msgid "qt6-multimedia"
msgstr "qt6-متن"

#: ../../../build/dependencies.md:113
msgid "qt6-networkauth"
msgstr "qt6- شبکه"

#: ../../../build/dependencies.md:114
msgid "qt6-svg"
msgstr "qt6-svg"

#: ../../../build/dependencies.md:115
msgid "qt6-tools"
msgstr "ابزار qt6"

#: ../../../build/dependencies.md:116
msgid ""
"qt6-webengine (optional, currently for link previews and some media file "
"previews)"
msgstr ""
"qt6-webengine (اختيار، در حال حاضر برای پیش نمایش لینک ها و برخی از پیش "
"نمایش فایل های رسانه ای)"

#: ../../../build/dependencies.md:119
msgid ""
"The libqt-jami and jami-libqt packages from "
"[dl.jami.net](https://dl.jami.net/) provide the required Qt 6 dependencies "
"on supported dpkg/Debian-based and RPM-based GNU/Linux distributions, "
"respectively. This is especially useful for building Jami on slightly older "
"versions of these distributions where Qt 6 is not yet packaged in the "
"distribution's official repositories. To install this package providing the "
"Qt 6 dependencies on a supported distribution:"
msgstr ""

#: ../../../build/dependencies.md:122
msgid ""
"Add the respective [dl.jami.net](https://dl.jami.net/) repository by "
"following the instructions on the [Download Jami for "
"GNU/Linux](https://jami.net/download-jami-linux) webpage; and"
msgstr ""

#: ../../../build/dependencies.md:123
msgid ""
"Install the package using the Terminal command `sudo apt-get install libqt-"
"jami` (for dpkg/Debian-based GNU/Linux distributions) or `sudo dnf install "
"jami-libqt` (for RPM-based GNU/Linux distributions)."
msgstr ""

#: ../../../build/dependencies.md:125
msgid ""
"On dpkg/Debian-based GNU/Linux distributions (such as Debian, Trisquel, "
"Ubuntu, and Linux Mint), the packages can be installed with `apt-get` or "
"`apt` as follows:"
msgstr ""

#: ../../../build/dependencies.md:144 ../../../build/dependencies.md:163
msgid "To install the optional Qt WebEngine packages:"
msgstr ""

#: ../../../build/dependencies.md:153
msgid ""
"On RPM-based GNU/Linux distributions (such as Fedora and openSUSE), the "
"packages can be installed with `dnf`, `yum`, or `zypper` as follows:"
msgstr ""

#: ../../../build/dependencies.md:170
msgid ""
"The easiest approach would be to install the libqt-jami or jami-libqt "
"package on your supported dpkg/Debian-based or RPM-based GNU/Linux "
"distribution, respectively. This would ensure that:"
msgstr ""

#: ../../../build/dependencies.md:172
msgid "All the required Qt 6 dependencies are fetched; and"
msgstr ""

#: ../../../build/dependencies.md:173
msgid ""
"The application is as similar as possible at what is being built and tested."
msgstr ""

#: ../../../build/index.md:1
msgid "Build manual"
msgstr "دستور کار ساخت"

#: ../../../build/index.md:3
msgid ""
"The Jami build manual documents the various aspects of building Jami from "
"source. It includes how to package Jami for various systems."
msgstr ""

#: ../../../build/index.md:6
msgid "This manual is for anyone looking to build Jami from source to:"
msgstr ""

#: ../../../build/index.md:7
msgid "Hack on Jami and contribute to its development;"
msgstr ""

#: ../../../build/index.md:8
msgid "Test new and unreleased features; and"
msgstr ""

#: ../../../build/index.md:9
msgid ""
"Package or maintain a Jami package in a GNU/Linux distribution repository."
msgstr ""

#: ../../../build/index.md:11
msgid "Sections"
msgstr ""

#: ../../../build/introduction.md:1
msgid "Introduction"
msgstr "مقدمه"

#: ../../../build/introduction.md:3
msgid "A Jami installation typically has two main components:"
msgstr ""

#: ../../../build/introduction.md:5
msgid "the Jami daemon/library, and"
msgstr "ديمون/کتابخانه Jami و"

#: ../../../build/introduction.md:6
msgid "the client (i.e., front-end or user interface)."
msgstr ""

#: ../../../build/introduction.md:8
msgid ""
"To use Jami, the Jami daemon/library is always needed, since it is the core "
"of Jami and contains all of the connectivity, communication, cryptography, "
"and media logic. It uses libraries such as OpenDHT, PJSIP, GnuTLS, and "
"FFmpeg and has several APIs, including DBus, libwrap (shared library), JNI, "
"and REST. These APIs make it possible to interact with Jami without going "
"through a graphical user interface (especially useful for using Jami in a "
"headless/server setting), build automation tools/scripts around Jami, and "
"build custom user interfaces for Jami."
msgstr ""

#: ../../../build/introduction.md:12
msgid ""
"The client (i.e., user interface) depends on the operating system and/or "
"platform being used. For example, on Android/Replicant systems this would be"
" [jami-client-android](https://git.jami.net/savoirfairelinux/jami-client-"
"android), on iOS systems this would be [jami-client-"
"ios](https://git.jami.net/savoirfairelinux/jami-client-ios), and on "
"GNU/Linux, macOS, and Windows this would be [jami-client-"
"qt](https://git.jami.net/savoirfairelinux/jami-client-qt). There also used "
"to be a GTK-based jami-client-gnome for GNU/Linux and a jami-client-macos "
"for macOS, both of which were deprecated in favor of the newer cross-"
"platform jami-client-qt based on the Qt framework."
msgstr ""

#: ../../../build/introduction.md:18
msgid ""
"On GNU/Linux systems, jami-client-qt can be configured and built to use one "
"of two main APIs for communicating with the Jami daemon/library:"
msgstr ""
"در سیستم های GNU/Linux، jami-client-qt می تواند برای استفاده از یکی از دو "
"API اصلی برای ارتباط با Jami daemon / کتابخانه پیکربندی و ساخته شود:"

#: ../../../build/introduction.md:20
msgid ""
"libwrap: When jami-client-qt is configured to use libwrap (which is always "
"the case on macOS and Windows), it will use Jami daemon's shared library "
"API, and there will be no separate daemon process. This has the advantage of"
" things being somewhat more efficient than with the DBus API mentioned "
"below, in exchange for less flexibility (not being able to interact with "
"Jami via DBus when using libwrap)."
msgstr ""

#: ../../../build/introduction.md:23
msgid ""
"The Jami packages distributed via dl.jami.net are currently all configured "
"to use the libwrap API."
msgstr ""
"بسته های Jami که از طریق dl.jami.net توزیع می شوند در حال حاضر همه برای "
"استفاده از API libwrap پیکربندی شده اند."

#: ../../../build/introduction.md:25
msgid ""
"DBus: When jami-client-qt is configured to use DBus, it will communicate "
"with the Jami daemon via its DBus API, and the daemon will be running as a "
"separate process by itself. This is greatly flexible for communicating with "
"and controlling the daemon through other means (for example, small utility "
"scripts) simultaneously while the Qt-based Jami client also uses and "
"interacts with it. The associated cost of this added flexibility is the "
"overhead of using DBus."
msgstr ""
