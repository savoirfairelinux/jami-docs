# Copyright (C) 2022 Savoir-faire Linux Inc.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

# for a full list of Sphinx configuration options, see:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

import sphinx_rtd_theme
from datetime import datetime

current_year = datetime.now().year

project = 'Jami'
copyright = f'2018-{current_year} Savoir-faire Linux Inc. and contributors'
author = 'Savoir-faire Linux Inc. and contributors'
html_baseurl = 'https://docs.jami.net'

extensions = [
    'myst_parser',
    'sphinx.ext.autosectionlabel',
    'sphinx.ext.extlinks',
    'sphinx.ext.mathjax',
    'sphinx_rtd_theme',
]

myst_enable_extensions = ['dollarmath']

extlinks = {
    # GitLab
    'daemon-issue':
    ('https://git.jami.net/savoirfairelinux/jami-daemon/-/issues/%s',
     'jami-daemon#%s'),
    'client-qt-issue':
    ('https://git.jami.net/savoirfairelinux/jami-client-qt/-/issues/%s',
     'jami-client-qt#%s'),
    'client-gnome-issue':
    ('https://git.jami.net/savoirfairelinux/jami-client-gnome/-/issues/%s',
     'jami-client-gnome#%s'),
    'client-android-issue':
    ('https://git.jami.net/savoirfairelinux/jami-client-android/-/issues/%s',
     'jami-client-android#%s'),
    'client-ios-issue':
    ('https://git.jami.net/savoirfairelinux/jami-client-ios/-/issues/%s',
     'jami-client-ios#%s'),
    'client-macos-issue':
    ('https://git.jami.net/savoirfairelinux/jami-client-macos/-/issues/%s',
     'jami-client-macos#%s'),
    'jami-web-issue':
    ('https://git.jami.net/savoirfairelinux/jami-web/-/issues/%s',
     'jami-web#%s'),
    'plugins-issue':
    ('https://git.jami.net/savoirfairelinux/jami-plugins/-/issues/%s',
     'jami-plugins#%s'),
    'nameservice-issue':
    ('https://git.jami.net/savoirfairelinux/jami-nameservice/-/issues/%s',
     'jami-nameservice#%s'),
    'packaging-issue':
    ('https://git.jami.net/savoirfairelinux/jami-packaging/-/issues/%s',
     'jami-packaging#%s'),
    'project-issue':
    ('https://git.jami.net/savoirfairelinux/jami-project/-/issues/%s',
     'jami-project#%s'),
    'gitlab-project':
    ('https://git.jami.net/savoirfairelinux/%s', 'gitlab:%s'),
    # Gerrit
    'gerrit': ('https://review.jami.net/%s', 'gerrit#%s'),
    'gerrit-project':
    ('https://review.jami.net/q/project:%s', 'gerrit:%s'),
}

source_suffix = {
    '.rst': 'restructuredtext',
    '.md': 'markdown',
}

autosectionlabel_prefix_document = True
autosectionlabel_maxdepth = 4

templates_path = ['_templates']

exclude_patterns = ['_build', '*env*', 'README.md']

html_theme = 'sphinx_rtd_theme'

html_theme_options = {
    'navigation_depth': 3,
    'sticky_navigation': False,
    'collapse_navigation': False,
}

# paths containing custom static files
# (copied after the builtin static files, so a "default.css"
#  file there would overwrite the builtin "default.css")
html_static_path = ['_static']

# internationalization / translations
locale_dirs = ['locales']
gettext_compact = True

languages = {
    'العربية': 'ar',
    'Български': 'bg',
    'বাংলা': 'bn',
    'Català': 'ca',
    'Dansk': 'da',
    'Deutsch': 'de',
    'Ελληνικά': 'el',
    'English (US)': 'en_US',
    'Esperanto': 'eo',
    'Español': 'es',
    'Eesti': 'et',
    'Euskara': 'eu',
    'فارسی': 'fa',
    'Suomi': 'fi',
    'Français': 'fr',
    'עברית': 'he',
    'हिन्दी': 'hi',
    'हिन्दी (भारत)': 'hi_IN',
    'Hrvatski': 'hr',
    'magyar': 'hu',
    'Indonesia': 'id',
    'Italiano': 'it',
    '日本語': 'ja',
    '한국어': 'ko',
    'Lietuvių': 'lt',
    'नेपाली': 'ne',
    'Nederlands': 'nl',
    'Polski': 'pl',
    'Português': 'pt',
    'Português (Brasil)': 'pt_BR',
    'Português (Portugal)': 'pt_PT',
    'Română': 'ro',
    'Русский': 'ru',
    #'සිංහල': 'si',  # TODO: Enable this language when translations are provided
    'Slovenčina': 'sk',
    'Slovenščina': 'sl',
    'Српски': 'sr',
    'Svenska': 'sv',
    'தமிழ்': 'ta',
    'తెలుగు': 'te',
    'Türkçe': 'tr',
    'Tiếng Việt': 'vi',
    '中文简体': 'zh_CN',
    '中文繁體': 'zh_TW',
}

html_context = {
    'languages' : list(languages.items()),
}