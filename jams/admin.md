# Admin guide

By default, the [Jami Account Management Server (JAMS)](https://jami.biz/) runs an embedded [Apache Tomcat](https://tomcat.apache.org/) server visible on port 8080.
However, this is impractical for many reasons.
This guide is designed to help with setting up a JAMS to run in a production environment.

## JAMS and Nginx

It is generally not recommended to expose JAMS directly to the outside world.
JAMS is required to run in SSL mode.
It is recommended to place JAMS behind an [Nginx](https://nginx.org/) or similar web server.
The Nginx, or similar, web server would proxy requests between the outside world and JAMS.

The following is an example map of how the JAMS could be configured behind an Nginx server.
The process would be similar if any other type of proxy solution is used.

![«Image: JAMS: Create admin account»](images/map.png "JAMS: Create admin account")

The IP 10.10.0.1 is random and should be seen as an example.

Typically a new site called `jams-site.conf` would be added to the Nginx configuration.
It would contain the following entries if an SSL certificate was placed at the Nginx level:
<pre>
server {
  listen 443 ssl;
  listen [::]:443 ssl;
  ssl on;
  ssl_certificate /etc/certificates/mycertificate.pem
  ssl_certificate_key /etc/certificates/mycertificatekey.pem
  ssl_client_certificate /jams/installation/path/CA.pem;
  ssl_verify_client optional;
  ssl_verify_depth 2;
  client_max_body_size 100M;
  server_name jams.mycompany.com;
  location / {
    # Block client-supplied headers that could be used to spoof
    if ($http_x_client_cert) {
      return 400;
    }
    proxy_pass        http://10.10.0.1:8080/;
    proxy_set_header  X-Real-IP $remote_addr;
    proxy_set_header  Host $http_host;
    proxy_set_header  X-Client-Cert $ssl_client_escaped_cert;
  }
}</pre>

This is the preferred setup method by most admins, as local traffic is usually run unencrypted since it is usually either an inter-VM connection, a VLAN, or another dedicated link.

```{note}
Since the CA is generated during the JAMS initial configuration, Nginx needs to be restarted once the initial setup is completed.
```

## Troubleshooting and resetting

If a restart from 0 (i.e., reset everything and drop existing data) is required, delete the following files in the distribution folder (`<project-root-folder>/jams`):
<pre>
The internal JAMS folder: &lt;project-root-folder>/jams/jams
derby.log
oauth.key
oauth.pub
config.json
</pre>

This will reset the server to its original state, and the configuration wizard is able to be run again.
Before performing this operation, please ensure that the server is shut down.

## Running JAMS as a GNU/Linux Service

Running JAMS as a GNU/Linux Service is fairly straightforward with systemd—simply create a service unit file with the following structure:
<pre>
<b>[Unit]</b>
Description=JAMS Server

<b>[Service]</b>
Type=simple
WorkingDirectory=[DIRECTORY WHERE JAMS WAS UNZIPPED]
ExecStart=/usr/bin/java -jar [DIRECTORY WHERE JAMS WAS UNZIPPED]/jams-launcher.jar PORT SSL_CERTIFICATE SSL_CERTIFICATE_KEY

<b>[Install]</b>
WantedBy=multi-user.target
</pre>

The parameters **PORT**, **SSL_CERTIFICATE** and **SSL_CERTIFICATE_KEY** are optional (however, **PORT** can be used alone, whereas the **SSL_CERTIFICATE** comes in a pair with **SSL_CERTIFICATE_KEY**).

## Running JAMS as a Windows Service

### A. Download and install JAMS

1. Visit <https://jami.biz/> and download JAMS.

2. Extract JAMS to C:\jams

### B. Download and install Java Development Kit (JDK)

1. Download JDK 11 from <https://www.oracle.com/java/technologies/javase-jdk11-downloads.html> (choose the corresponding VM architecture).

2. Install it using the installation wizard.

### C. Download OpenSSL to generate a key and a certificate

1. Download the OpenSSL Binary Distributions for Microsoft Windows from <https://kb.firedaemon.com/support/solutions/articles/4000121705>.
   Alternatively, select another [OpenSSL binary](https://wiki.openssl.org/index.php/Binaries).

2. Once downloaded, extract it to `C:\openssl`.

3. Create a `bin` folder inside, i.e., `C:\openssl\bin`.

4. Create a new file inside the `bin` folder named `openssl.cnf` (make sure that the file extension is `.cnd` and not `.cnd.txt`).

5. Copy and paste the following default configuration from <http://www.flatmtn.com/article/setting-openssl-create-certificates.html>:
   <pre>
   <i>#
   # OpenSSL configuration file.
   #
   
   # Establish working directory.
   </i>
   dir                         = .
   
   <b>[ca]</b>
   default_ca                  = CA_default
   
   <b>[CA_default]</b>
   serial                      = $dir/serial
   database                    = $dir/certindex.txt
   new_certs_dir               = $dir/certs
   certificate                 = $dir/cacert.pem
   private_key                 = $dir/private/cakey.pem
   default_days                = 365
   default_md                  = md5
   preserve                    = no
   email_in_dn                 = no
   nameopt                     = default_ca
   certopt                     = default_ca
   policy                      = policy_match
   
   <b>[policy_match]</b>
   countryName                 = match
   stateOrProvinceName         = match
   organizationName            = match
   organizationalUnitName      = optional
   commonName                  = supplied
   emailAddress                = optional
   
   <b>[req]</b>
   default_bits                = 1024      <i># Size of keys</i>
   default_keyfile             = key.pem   <i># Name of generated keys</i>
   default_md                  = md5       <i># Message digest algorithm</i>
   string_mask                 = nombstr   <i># Permitted characters</i>
   distinguished_name          = req_distinguished_name
   req_extensions              = v3_req
   
   <b>[req_distinguished_name]</b><i>
   # Variable name               Prompt string
   #--------------------------   -----------------------------------------------</i>
   0.organizationName          = Organization Name (company)
   organizationalUnitName      = Organizational Unit Name (department, division)
   emailAddress                = Email Address
   emailAddress_max            = 40
   localityName                = Locality Name (city, district)
   stateOrProvinceName         = State or Province Name (full name)
   countryName                 = Country Name (2 letter code)
   countryName_min             = 2
   countryName_max             = 2
   commonName                  = Common Name (hostname, IP, or username)
   commonName_max              = 64
   
   <i># Default values for the above, for consistency and less typing.
   # Variable name               Value
   #--------------------------   -----------------------------------------------</i>
   0.organizationName_default  = My Company
   localityName_default        = My Town
   stateOrProvinceName_default = State or Province
   countryName_default         = US
   
   <b>[v3_ca]</b>
   basicConstraints            = CA:TRUE
   subjectKeyIdentifier        = hash
   authorityKeyIdentifier      = keyid:always,issuer:always
   
   <b>[v3_req]</b>
   basicConstraints            = CA:FALSE
   subjectKeyIdentifier        = hash
   </pre>

### D. Add OpenSSL to System Environment Variables

Go to Edit the system environment variables → Environment Variables.
In System variables, edit **Path** and add `C:\openssl\`.

### E. Configure OpenSSL

1. Open the **Command Prompt**.

2. Execute the following command to set the path to the OpenSSL configuration file.  
   `set OPENSSL_CONF=C:\openssl\bin\openssl.cnf`

3. Type `cd C:\jams`

4. To generate the **Key** and **Certificate**, type:  
   `openssl req -newkey rsa:2048 -new -nodes -x509 -days 3650 -keyout server.key -out server.pem`

5. Follow the wizard.

6. Once the key and certificate are generated, type `dir`.
   The output should look like:
   <pre>
   C:\jams>dir
    Volume in drive C has no label.
    Volume Serial Number is BC94-9EF2<br>
    Directory of C:\jams<br>
   2020-11-10  12:38 PM    &lt;DIR>          .
   2020-11-10  12:38 PM    &lt;DIR>          ..
   2020-10-22  10:56 AM         5,186,016 jams-launcher.jar
   2020-10-22  10:56 AM        33,413,882 jams-server.jar
   2020-11-10  11:53 AM    &lt;DIR>          libs
   2020-11-10  12:34 PM             1,732 server.key
   2020-11-10  12:38 PM             1,336 server.pem
   2020-10-22  04:05 PM         2,047,932 userguide.pdf
               5 File(s)       40,650,898 bytes
               3 Dir(s)    93,365,936,128 bytes free
   </pre>

7. Execute the following command to start JAMS:  
   `java -jar jams-launcher.jar PORT_NUMBER (eg. 8443 or 443) server.pem server.key`

8. Open a navigator on the server and visit <https://localhost:443> or <https://localhost:8443> to validate that JAMS is working.

9. Type CTRL+C to close the application.

### F. Expose the localhost to the Internet

1. Press the Windows key or click the Windows icon and search for ***Windows Defender Firewall with Advanced Security***.

2. Right-click **Inbound Rules** and click **New Rule…**

3. Select **Port**, click **Next**.

4. Specify the port to use, for example, ***443*** or ***8443***, and click **Next**.

5. Select **Allow the connection** and click **Next**.

6. Leave Domain Private and Public unchanged, and click **Next**.

7. Name the rule ***JAMS Inbound*** and click **Finish**.

8. Right-click on **Outbound Rules** and click **New Rule…**

9. Select **Port**, click **Next**.

10. Specify the port to use, for example, ***443*** or ***8443***, and click **Next**.

11. Select **Allow the connection** and click **Next**.

12. Leave Domain Private and Public unchanged, and click **Next**.

13. Name the rule ***JAMS Outbound*** and click **Finish**.

14. The localhost is now available on the Internet.
   The application can now be visited through the server domain name or IP address on port 443 or 8443.

### G. Create a JAMS Windows Service (Embed Tomcat Server Windows Service) to start JAMS with the server

1. In order to create a JAMS Windows Service, the **NSSM (the Non-Sucking Service Manager)** can be used.
   NSSM is available at <http://nssm.cc/download> and <https://github.com/kirillkovalenko/nssm>.

2. Once NSSM has successfully downloaded, open a **Command Prompt** and change the directory to:
   <pre>
   nssm-2.24\win64
   </pre>

3. To install and open a graphical user interface (GUI), type:
   <pre>
   nssm.exe install JAMS
   </pre>

4. In the **Path** field, specify the path to the Java executable, for example:
   <pre>
   "C:\Program Files\Common Files\Oracle\Java\javapath\java.exe"
   </pre>

5. In the **Startup directory**, for the installation folder path, type:
   <pre>
   "C:\jams"
   </pre>

6. In the last field, add the following arguments:
   <pre>
   -classpath "C:\jams" -jar jams-launcher.jar PORT_NUMBER server.pem server.key
   </pre>
   where **PORT_NUMBER** is the port number to use to serve the application, for example, ***443*** or ***8443***.

7. Now the JAMS application will start with the server.

Source: <https://medium.com/@lk.snatch/jar-file-as-windows-service-bonus-jar-to-exe-1b7b179053e4>