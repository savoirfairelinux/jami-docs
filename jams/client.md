# Client guide

This tutorial has instructions on how to connect to the JAMS (Jami Account Management Server) with Jami desktop (GNU/Linux, macOS, and Windows), Jami mobile (Android and iOS), and Jami web clients.

For the purposes of this tutorial, it is assumed that:

1. The server and the device attempting to connect are either:
    * On the same network, or
    * The server is publicly accessible to the outside world.
2. A valid username/password pair to connect to the server is entered.

## Connect with desktop clients

Jami desktop clients are available for devices with 64-bit versions of GNU/Linux, macOS, and Windows operating systems.

In Jami, open the **Add another account** page. Select **Advanced features**.

![«Image: Connect to JAMS with Jami desktop clients for GNU/Linux, macOS, and Windows devices: Step 1»](images/connect-to-jams-with-jami-desktop-client-step-1.png "Connect to JAMS with Jami desktop clients for GNU/Linux, macOS, and Windows devices: Step 1")

Select **Connect to a JAMS server**.

![«Image: Connect to JAMS with Jami desktop clients for GNU/Linux, macOS, and Windows devices: Step 2»](images/connect-to-jams-with-jami-desktop-client-step-2.png "Connect to JAMS with Jami desktop clients for GNU/Linux, macOS, and Windows devices: Step 2")

The **Jami Account Management Server URL** is the DNS address of the server.
The **Username** and **Password** correspond to the account.
If a server has been configured with an LDAP/AD backend, the **Username** and **Password** would be the LDAP/AD username and password.

## Connect with Android clients

Jami mobile clients are available for devices for smartphones, tablets, and TVs running the Android operating system.

In Jami, open the **Add another account** page.

![«Image: Connect to JAMS with Jami mobile clients for Android (phones, tablets, and TV) devices: Step 1»](images/connect-to-jams-with-jami-android-client-step-1.png "Connect to JAMS with Jami mobile clients for Android (phones, tablets, and TV) devices: Step 1")

Select **Connect to management server**.

![«Image: Connect to JAMS with Jami mobile clients for Android (phones, tablets, and TV) devices: Step 2»](images/connect-to-jams-with-jami-android-client-step-2.png "Connect to JAMS with Jami mobile clients for Android (phones, tablets, and TV) devices: Step 2")

The **Jami Account Management Server URL** is the DNS address of the server.
The **Username** and **Password** correspond to the account.
If a server has been configured with an LDAP/AD backend, the **Username** and **Password** would be the LDAP/AD username and password.

## Connect with iOS clients

Jami mobile clients are available for devices for iPhones and iPads running the iOS operating system.

In Jami, open the **Add another account** page.

![«Image: Connect to JAMS with Jami mobile clients for iOS (iPhone and iPad) devices: Step 1»](images/connect-to-jams-with-jami-ios-client-step-1.png "Connect to JAMS with Jami mobile clients for iOS (iPhone and iPad) devices: Step 1")

Select **Connect to Jami Account Management Server (JAMS)**.

![«Image: Connect to JAMS with Jami mobile clients for iOS (iPhone and iPad) devices: Step 2»](images/connect-to-jams-with-jami-ios-client-step-2.png "Connect to JAMS with Jami mobile clients for iOS (iPhone and iPad) devices: Step 2")

The **JAMS URL** is the DNS address of the server.
The **Username** and **Password** correspond to the account.
If a server has been configured with an LDAP/AD backend, the **Username** and **Password** would be the LDAP/AD username and password.

## Connect with web clients

Jami web clients run on modern [web browsers](https://en.wikipedia.org/wiki/Web_browser).

In a web browser [address bar (also location bar or URL bar)](https://en.wikipedia.org/wiki/Address_bar), enter the Jami Account Manager Server URL address.

If the JAMS administrator has disabled Jami authentication, enter the **Username** and **Password**.

![«Image: Connect to JAMS with web clients: Jami authentication disabled»](images/connect-to-jams-with-jami-web-client-authentication-disabled.png "Connect to JAMS with web clients: Jami authentication disabled")

If the JAMS administrator has enabled Jami authentication, the **Jami** or **JAMS** authentication method is required.

![«Image: Connect to JAMS with web clients: Jami authentication enabled»](images/connect-to-jams-with-jami-web-client-authentication-enabled.png "Connect to JAMS with web clients: Jami authentication enabled")

Select **Log in**.