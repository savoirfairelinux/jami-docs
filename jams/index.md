![«Image: JAMS logo»](images/logo-jams.png "JAMS logo")

# JAMS manual

![«Image:Jami Account Management Server (JAMS)»](images/jams-hero.png "Jami Account Management Server (JAMS)")

The JAMS manual provides a background of and contains the admin and user guides for the Jami Account Management Server.
The Jami Account Management Server (JAMS) enables Jami to be easily deployed in any enterprise and allows users to connect using their centralized credentials and create local accounts.
JAMS allows all enterprises to manage their own Jami community while taking advantage of Jami's distributed network architecture.

## Download JAMS manual

| Language     | Print file format         | Source file format        |
| :----------- | :-----------------------: | :-----------------------: |
| en (English) | [PDF](manual/jams-en.pdf) | [ODT](manual/jams-en.odt) |

## Additional guides

```{toctree}
:maxdepth: 1

admin
client
```

## Introduction

JAMS is a server application used to enroll Jami clients in an enterprise environment.
Currently, JAMS supports 3 sources for user authentication:
1. Lightweight Directory Access Protocol (LDAP),
2. Active Directory (AD), and
3. An embedded database.

### Obtaining JAMS

The latest version of JAMS can be downloaded at <https://jami.biz/>.
The source code is available at <https://git.jami.net/savoirfairelinux/jami-jams>.

### System requirements

<table>
<thead>
<tr>
<th>Requirement</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Processor</strong></td>
<td>1 gigahertz (GHz) or faster with 1 or more cores on a compatible 64-bit processor or System on a Chip (SoC).</td>
</tr>
<tr>
<td><strong>RAM</strong></td>
<td>4 gigabytes (GB).</td>
</tr>
<tr>
<td><strong>Storage</strong></td>
<td>1 gigabyte (GB) of available storage space.</td>
</tr>
<tr>
<td><strong>Operating system</strong></td>
<td>GNU/Linux, Apple macOS, or Microsoft Windows.</td>
</tr>
<tr>
<td><strong>Java</strong></td>
<td>Version 11 or higher.</td>
</tr>
<tr>
<td><strong>Database</strong></td>
<td>LDAP-compatible directory (such as <a href="https://www.openldap.org/">OpenLDAP</a>), Microsoft Active Directory (AD), or a local embedded database.</td>
</tr>
</tbody>
</table>

### JAMS concepts

JAMS was built with security in mind; therefore, it is intimately related to the [X.509](https://en.wikipedia.org/wiki/X.509) certificate management workflows.

The central concepts that are used in JAMS are:

* the [Certification Authority (CA)](https://en.wikipedia.org/wiki/Certificate_authority) and
* the [Certificate Signing Request (CSR)](https://en.wikipedia.org/wiki/Certificate_signing_request).

In the JAMS paradigm, a device (Jami client) requests a certificate from the server and then presents it to other devices to be recognized as a valid member of the organization.
Therefore, JAMS must be provided with a certificate authority in order to work properly.

In order to be completely secure, JAMS does not generate certificates for devices.
JAMS instead issues certificates based on a certificate signing request sent to it by the device.
This therefore removes the requirement to send a private key over the wire.

The following diagram shows the entire process of how a device enrolls with JAMS:

![«Image: Device enrollment»](images/device_enroll.png "Device enrollment")

### Getting started

1. Download the latest version of JAMS from <https://jami.biz/>.

2. Unpack the `.tar` file to any directory.

3. It is mandatory to run JAMS using a secure SSL connection.

A domain name is required to request a key and a certificate.
A domain name can be purchased if one is not available.
Set the domain name to point to the server before proceeding to the next step.

A pair of key certificates can be purchased from any online provider.
However, obtaining a free pair using **[Let's Encrypt](https://letsencrypt.org/)** is recommended.

In order to generate a pair of key certificates, **Certbot** can be used following the instructions on the <https://certbot.eff.org/> page.

Certbot will provide specific instructions when the web server software and operating system are entered.

Install Certbot using snap:
```
sudo snap install --classic certbot
```

Ensure that the Certbot command can be run:
```
sudo ln -s /snap/bin/certbot /usr/bin/certbot`
```

In order to get a certificate, execute:
```
sudo certbot certonly
```
and follow the instructions.

The certificate and key are generated in a specific folder; please see the output from Certbot to locate them.

It is required to copy them in the current folder where the `jams-launcher.jar` file is located.

```{admonition} Current limitation
JAMS currently does not support reading encrypted private keys that require a password unlock.
```

4. Navigate to the directory where the JAMS package has been extracted and execute the following command:
```
java -jar jams-launcher.jar PORT SSL_CERTIFICATE SSL_CERTIFICATE_KEY
```

<table>
<thead>
<tr>
<th>Argument</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>PORT</strong></td>
<td>The TCP port on which JAMS is to listen for incoming connections.</td>
</tr>
<tr>
<td><strong>SSL_CERTIFICATE</strong></td>
<td>The location of the PEM-formatted SSL Certificate file.</td>
</tr>
<tr>
<td><strong>SSL_CERTIFICATE_KEY</strong></td>
<td>The location of the PEM-formatted key file, which is used with the SSL Certificate file from above.</td>
</tr>
</tbody>
</table>

An example of the command would be:
```
java -jar jams-launcher 443 server.pem server.key
```

```{note}
Any port above 1024 can be safely used to run JAMS.
```

### Step 1: Create an administrator account

The administrator account manages Jami users and groups.

![«Image: JAMS: Create admin account»](images/jams-step-1.png "JAMS: Create admin account")

### Step 2: Set up the Certification Authority

The second step is to define the certification authority.

```{important}
A CA is not a server SSL certificate; it is a certificate that has the power to issue other certificates.
Do not use the import option unless the enterprise's security officer has issued the CA certificate.
Most commercially available certificates (i.e., those issued by GoDaddy, Let’s Encrypt, etc.) are not CA certificates.
It is highly recommended that end-users create and use a self-signed CA, as providing an incorrect certification type will lead to a non-functional server.
```

![«Image: JAMS: Create Certification Authority»](images/jams-step-2-1.png "JAMS: Create Certification Authority")

![«Image: JAMS: Import Certification Authority»](images/jams-step-2-2.png "JAMS: Import Certification Authority")

This certificate will be used to sign the enrollment requests that come from Jami devices.
It is highly recommended that the following articles are read to become familiar with the X.509 certificate standard processes and practices:

* <https://www.securew2.com/blog/public-key-infrastructure-explained/>
* <https://cheapsslsecurity.com/blog/understanding-the-role-of-certificate-authorities-in-pki/>

### Step 3: Set up the user database

JAMS supports 3 different sources for the authentication of users:
1. LDAP-compatible directory (such as [OpenLDAP](https://www.openldap.org/))
2. Microsoft Active Directory
3. Local embedded database

#### Option 1: Lightweight Directory Access Protocol (LDAP)

If the enterprise provides an LDAP directory for user management, it is required to know its access information and an automated account that has read-only rights to do use look-ups.

![«Image: JAMS: Lightweight Directory Access Protocol (LDAP)»](images/jams-ldap.png "JAMS: Lightweight Directory Access Protocol (LDAP)")

The admin should provide most of the required information; however, the following is a detailed overview of each field:

<table>
<thead>
<tr>
<th>Field</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Use StartTLS</strong></td>
<td>The LDAP server can be configured to use either TLS/STARTTLS or PLAIN sockets; if STARTTLS is used, mark the value as true.</td>
</tr>
<tr>
<td><strong>Server Address</strong></td>
<td>The address of the server with respect to the JAMS server, the LDAP is not required to be publicly accessible but should be accessible to JAMS.
  Either <code>ldap://</code> or <code>ldaps://</code> should precede the address.</td>
</tr>
<tr>
<td><strong>Port</strong></td>
<td>The port on which the LDAP server is listening for requests (usually 389 for PLAIN/STARTTLS and 636 for SSL/TLS).</td>
</tr>
<tr>
<td><strong>Administrator Username</strong></td>
<td>This is <strong>NOT</strong> the LDAP's administration account credentials but the credentials of the account that has <i>read</i> permissions to the LDAP database in order to look up users.
  The format is generally <code>cn=bot,ou=robots,dc=domain,dc=org</code>.</td>
</tr>
<tr>
<td><strong>Password</strong></td>
<td>The password used by the account above.</td>
</tr>
<tr>
<td><strong>BaseDN</strong></td>
<td>The base realm where the user accounts are located; in most cases, it is <code>ou=users,dc=enterprise,dc=org</code>.</td>
</tr>
</tbody>
</table>

<br>

#### Option 2: Microsoft Active Directory (AD)

If the enterprise provides Active Directory (AD) for user management, it is required to know its access information and an automated account that has read-only rights to do use look-ups.

![«Image: JAMS: Active Directory (AD)»](images/jams-ad.png "JAMS: Active Directory (AD)")

The admin should provide most of the required information; however, the following is a detailed overview of each field:

<table>
<thead>
<tr>
<th>Field</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>Port</strong></td>
<td>The port on which Active Directory (AD) is listening (generally it is either 389 or 636).</td>
</tr>
<tr>
<td><strong>Host</strong></td>
<td>The address of the server with respect to the JAMS server, the Active Directory (AD).
  It not required to be publicly accessible but should be accessible to JAMS.</td>
</tr>
<tr>
<td><strong>Administrator Username</strong></td>
<td>This is <strong>NOT</strong> the Active Directory's administration account credentials but the credentials of the account that has <i>read</i> permissions to the Active Directory database in order to look up users.
  The format is generally <code>cn=bot,ou=robots,dc=domain,dc=net</code>.</td>
</tr>
<tr>
<td><strong>Password</strong></td>
<td>The password used by the account above.</td>
</tr>
<tr>
<td><strong>Use SSL</strong></td>
<td>Whether the server uses SSL for data transmission.</td>
</tr>
<tr>
<td><strong>Domain Name</strong></td>
<td>This is the legacy-formatted Windows Domain Name (i.e., <code>WINDOMAIN</code>).</td>
</tr>
</tbody>
</table>

#### Option 3: Local embedded database

The local database does not require any additional configuration; everything in the process is automated.
This option allows for the creation of Jami users on the fly directly from the JAMS interface.

![«Image: JAMS: Local database»](images/jams-local.png "JAMS: Local database")

```{admonition} Advanced settings
By default, the "Use public name server" option is disabled.
Jami usernames of JAMS users will not be stored on the public Jami name server.
Users can communicate with users outside the organization by using their 40-character fingerprint.
Enable this option to allow JAMS users in the organization to also search for external users on the public name server.
```

### Step 4: Set up the server parameters

![«Image: JAMS: Set up server parameters»](images/jams-step-4.png "JAMS: Set up server parameters")

<table>
<thead>
<tr>
<th>Parameter</th>
<th>Details</th>
</tr>
</thead>
<tbody>
<tr>
<td><strong>CORS Domain Name</strong></td>
<td>The domain on which the JAMS client and administration UI will be running.</td>
</tr>
<tr>
<td><strong>Certificate Revocation List Lifetime</strong></td>
<td>The frequency at which the CRL is updated in memory.</td>
</tr>
<tr>
<td><strong>Device Lifetime</strong></td>
<td>How long a device's certificate is valid before being considered stale and requiring re-enrollment.</td>
</tr>
<tr>
<td><strong>User Account Lifetime</strong></td>
<td>How long a user account is valid before being considered stale and requiring re-enrollment.</td>
</tr>
</tbody>
</table>

```{important}
The *CORS Domain Name* corresponds to the web address used to access the Web UI.
By default, it is set to the same URL address as the one where JAMS is deployed.
Only set a different URL address if the Web UI has a different URL address from the one where JAMS is deployed.
```

Select the **Set Server Parameters** button to finalize the configuration and be redirected to the JAMS interface.

![«Image: JAMS dashboard»](images/jams-dashboard.png "JAMS dashboard")

If JAMS has been configured with an LDAP database or Active Directory (AD), the list of users in the organization should be visible in JAMS.
If JAMS has been configured with a local embedded database, new users can be created by selecting the **Create User** button.