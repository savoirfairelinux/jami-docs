# -*- mode: makefile; -*-
# Copyright (C) 2022 Savoir-faire Linux Inc.
#
# Copying and distribution of this file, with or without modification,
# are permitted in any medium without royalty provided the copyright
# notice and this notice are preserved.  This file is offered as-is,
# without any warranty.

CMD ?= sphinx-build
AUTOBUILD_CMD ?= sphinx-autobuild
INTL_CMD ?= sphinx-intl
TX_CMD ?= tx
OPTS ?=
SRC  = .
OUT  = _build/out
RSYNC_DEST ?= $(OUT)/docs
RSYNC_OPTS ?= --verbose --archive --recursive --delete

# Languages to build
LANGS = ar bg bn ca da de el en_US eo es et eu fa fi fr he hi hi_IN hr hu id it ja ko lt ne nl pl pt pt_BR pt_PT ro ru si sk sl sr sv ta te tr vi zh_CN zh_TW
space = $() $()
comma = ,
LANGS_commasep = $(subst $(space),$(comma),$(LANGS))

# default rule
help:
	@$(CMD) -M help "$(SRC)" "$(OUT)" $(OPTS)

.PHONY: help Makefile

# specific rule to use sphinx-autobuild
watch: Makefile
	@$(AUTOBUILD_CMD) -b html "$(SRC)" "$(OUT)"/html $(OPTS)

# Avoid manual (non-gettext) French translations getting picked up by
# by sphinx for translation.  This is temporary, until these pages are
# properly (re)translated into French and these files are dropped.
gettext-pre:
	-@for f in user/*-fr.md; do \
	  if [ -f "$$f" ]; then mv "$$f" "$$f.bk"; fi; \
	done
	-@for f in developer/*-fr.md; do \
	  if [ -f "$$f" ]; then mv "$$f" "$$f.bk"; fi; \
	done

gettext: gettext-pre

po-pre:
# shouldn't translate the GNU FDL
	-rm $(OUT)/gettext/fdl.pot
po: gettext po-pre
	@for l in $(LANGS); do \
	  $(INTL_CMD) update -p "$(OUT)"/gettext -l $$l; \
	done

tx-pull:
#	$(TX_CMD) pull -aft --minimum-perc=1
#   e.g. make tx-pull TRANSIFEX_API_TOKEN=<token_value>
	$(TX_CMD) pull -ftl $(LANGS_commasep)

tx-push:
	$(TX_CMD) push -s

clean:
	@echo "Removing $(OUT)"
	rm -r $(OUT)
deploy:
	rsync $(RSYNC_OPTS) "$(OUT)"/html/ "$(RSYNC_DEST)"
	for l in $(LANGS); do \
	  rsync $(RSYNC_OPTS) "$(OUT)/$$l"/html/ "$(RSYNC_DEST)"/"$$l"; \
	done

# anything else
%: Makefile
	@$(CMD) -M $@ "$(SRC)" "$(OUT)" $(OPTS)
	@for l in $(LANGS); do \
	  $(CMD) -M $@ "$(SRC)" "$(OUT)/$$l" \
	    -D language="$$l" \
	    -D html_baseurl="https://docs.jami.net/$$l" \
	    $(OPTS); \
	done

.PHONY: gettext-pre po-pre po tx-pull tx-push deploy
