# Contribute

Contributions to Jami are always welcome and are much appreciated.
There are many ways to contribute to Jami, including:
* Reporting bugs and issues,
* Contributing code,
* Helping package and maintain Jami for your GNU/Linux distribution or other operating system,
* Contributing to the [Jami documentation](https://docs.jami.net/).

Please see below for how to get started contributing to Jami!

## Reporting bugs and issues

Please see the [Bug report guide](user/bug-report-guide.md) for step-by-step instructions on how to report any issue you may encounter with Jami.

## Contributing code

To start contributing to Jami, look at the good first issues at:
<a href="https://git.jami.net/groups/savoirfairelinux/-/issues/?sort=created_date&state=opened&label_name%5B%5D=good%20first%20issue">
https://git.jami.net/groups/savoirfairelinux/-/issues/?sort=created_date&state=opened&label_name[]=good first issue
</a>.

Contact the developers directly by adding a comment on the ticket.
This will enable the developers to guide you through the process.

A push with a patch to <https://review.jami.net> will be required to integrate the code to Jami.

```{seealso}
For more information on how to push a patch, please refer to the [Working with Gerrit](developer/new-developers/working-with-gerrit.md) guide.
```

## Commit message guidelines

When submitting a patch to Jami, please follow the following guidelines for the commit messages:

1. The first line should include the component or scope of the change, followed by a short summary of the change in the imperative mood (e.g., "add new function", "fix bug", "update documentation").

3. The subject can use the capitalization of the component or scope, but the rest of the title should be lowercase.

4. The second line should be blank.

5. The third line should be the beginning of a longer description of the change in complete sentences, if necessary.

6. 50/72 rule: The first line should be no longer than 50 characters (ideally), and the rest of the message should be wrapped at 72 characters per line.
   This can be configured in your text editor.

7. If the change is related to a specific issue in the Jami GitLab, include the issue number in the commit message.
   For example: `GitLab: #123`.
   If the change is related to multiple issues, list them all.
   If the change is related to an issue that is not part of the project, use a link to the issue instead.

Template for a commit message:

```
<Component/Scope>: <short Summary (imperative, max 50 characters)>

<Detailed description (in present tense) of what was changed and why
it was necessary, wrapped at 72 characters per line to maintain
readability. Include any important details that help others understand
the context of the change. Use this space to explain complex changes
or provide background information.>

[GitLab: #<issuenumber>] or [Link to issue]
```

For example:

```
ConversationView: add a new function

Adds a new function to the ConversationView class that allows
the user to sort conversations by date. This function is necessary
to improve the user experience and make it easier to find specific
conversations.

GitLab: #123
```

## Packaging Jami

There are two possible ways to package Jami:
1. Via our internal process to create packages available on the Snap Store or <https://dl.jami.net>.
2. Via the packaging process of your favorite GNU/Linux distribution.

```{important}
Jami is a quite complex project with a lot of dependencies.
This isn't a quick and easy task and requires maintenance.
```

```{note}
If Jami is packaged using the second option:
* For official releases, Jami uses certain [Qt](https://www.qt.io/) versions because Qt is a big dependency.
  This is to ensure that Jami works with the version we use.
  Every slight change in the Qt version can break Jami or bring some small unwanted changes.
  For example, 6.2 → 6.4 broke the video pipeline.
* Jami uses a fork of the [pjproject](https://github.com/pjsip/pjproject), as ICE over TCP is a requirement that is not planned upstream.
* [libupnp](https://pupnp.sourceforge.io/) received some patches to ensure that it's built with the non-blocking API.
* [FFmpeg](https://ffmpeg.org/) has some screen sharing patches.
* Visit `daemon/contrib/src` to check how dependencies are built.
```

For internal packaging, everything is located in `extras/packaging/gnu-linux`.
You can follow previous patches to understand your needs. e.g., <https://review.jami.net/c/jami-client-qt/+/28036>

We use 3 channels:
* Internal for testing purposes
* Nightly/Beta/Edge for public beta
* Stable for public releases

We generally use internal when we test new distributions or when we package a new Qt version.
Then we try to generate a nightly per week and one stable per month (if unit tests are green).

Packages are pushed to:
* [dl.jami.net](https://dl.jami.net/) (2 machines, with an rsync every 15 min)
* Ubuntu store ([snapcraft.io/jami](https://snapcraft.io/jami))

If you want to add a new distribution, you will need to:
* Add Dockerfile
* Change Makefile
* Update packaging script
* Cross fingers
* Test the package in a VM

```{warning}
Chromium is a hard part to build. Three common problems encountered are:

* GCC is too recent:
  * Generally the fix consist of importing patches from Chromium's gerrit to fix GCC issues
* Python is too recent:
  * Generally the fix consist of using PyEnv to get a virtual environment with the correct Python's version
* Missing dependencies:
  * During the configuration step of Qt, the list of built components is listed and the missing dependencies are listed. Generally installing a package or updating node.js fix the issue
  * Note that if Qt is generated without chromium, we must remove the package in the cache of the build machines to regenerate a new one (/var/cache/jami)
```

If you want to remove a distribution:
* If a distribution is EOL OR if there are 2 more recent LTS, we can remove the distribution (e.g., Ubuntu 20, 22, 24—remove Ubuntu 20) by removing related files and checks.

```{note}
The next big changes required are:
* Use [CMake](https://cmake.org/) instead of autotools for jami-daemon.
* Use [Ubuntu Core 22 (UC22)](https://ubuntu.com/core/docs/uc22) instead of core20 in snap.
* [Flatpak](https://flatpak.org/)/[AppImage](https://appimage.org/) support? This may simplify custom RPM/Debian packaging.
* Only generate one unified Debian file per distribution. Same for RPMs.
* Use [Jenkinsfile](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/) to generate packages for GNU/Linux, macOS, and Windows at the same time if possible.
```

For internal information (such as how to publish to stores, cf. internal wiki).

## Contributing to this documentation

Contributions to these docs are always welcome and appreciated, from small corrections to whole new chapters.

This page will walk through the steps to create a new page or submit a correction.
The patch review process is the same as for any other Jami project, so we will not explain every command.

```{note}
By contributing to this documentation, you agree to make your contributions available under the [fdl](fdl.md), Version 1.3 or any later version published by the Free Software Foundation; with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.

You are also promising that you are the author of your changes, or that you copied them from a work in the public domain or a work released under a free license that is compatible with the [fdl](fdl.md).
DO NOT SUBMIT COPYRIGHTED WORK WITHOUT PERMISSION.
```

```{seealso}
If you want to help to translate this page, you can join the project and start translating this page on
<https://explore.transifex.com/savoirfairelinux/>.
```

## Dependencies

Git is required to be installed and configured to use your SSH keypair and an account on the [Jami Gerrit](https://review.jami.net), where you would send your patches for review.
If you need help with this, see the beginning of our patch submission guide (TODO).

If you want to preview your changes locally in your web browser, you need to install:
* [Sphinx](https://www.sphinx-doc.org),
* [Read the Docs Sphinx theme](https://sphinx-rtd-theme.readthedocs.io/en/stable/), and
* [MyST Markdown parser](https://myst-parser.readthedocs.io/en/latest/index.html).

```bash
$ pip install --upgrade sphinx sphinx_rtd_theme myst_parser
```

If you want to use the auto-build and auto-refresh feature, also install
[sphinx-autobuild](https://github.com/executablebooks/sphinx-autobuild).

```bash
$ pip install --upgrade sphinx-autobuild
```

## Cloning the repository

Clone the repository and configure the push settings like this:

```bash
$ git clone "ssh://USERNAME@review.jami.net:29420/jami-docs.git"
$ cd jami-docs
$ git config remote.origin.push HEAD:refs/for/master
```

You may want to check out a new branch for each contribution/change before you make any change to the files so that you could easily `git pull` any future changes from upstream into your main local branch:

```bash
$ git checkout -b my-example-change
```

## Editing a page

Pages are written in [Markdown](https://www.markdownguide.org/).
Click "View page source" at the top of any page to open the raw source of the page and see how it was written.

Go ahead and make your changes to the `.md` files.

## Previewing your work

From the base of the repository, run:

```bash
$ make clean && make html
```

You should now be able to view the documentation in your web browser.
The homepage is at `_build/html/index.html`.

```{warning}
This documentation does not currently build with the latest version of [Sphinx](https://www.sphinx-doc.org/).
Please see [this issue on GitLab](https://git.jami.net/savoirfairelinux/jami-docs/-/issues/15)
for a workaround and updates regarding this problem.
```

To automatically build the documentation and refresh your web browser whenever you save changes, run:

```bash
$ make clean && make watch
```

Keep this running in the background, then navigate to `http://127.0.0.1:8000` (*not* the local .html file).

## Saving your work

```bash
$ git add source/file/you/edited.md
$ git commit
```

Refer to the [commit message guidelines](contribute.md#commit-message-guidelines) for how to write a good commit message.

## Submitting a change

The first time you try to push your changes, Gerrit will complain that you don't have a Change-Id in your commit, and provide an `scp` command to install the commit hook.
After running the command, you should be able to recommit and push your change:

```bash
$ git commit --amend --no-edit
$ git push
```

## Modifying your work

A reviewer may ask you to make changes to your patch before merging it.
This is no problem!
Simply make the changes, `git add` them, and run `git commit --amend` to modify the patch.

```{note}
The `--amend` switch, which is required to tell Git to *amend*/tweak the existing newest commit rather than making a new commit.
This is the workflow for updating a proposed change when using Gerrit.
```

## Adding a page

If you decide to add a whole new page to the documentation, you must also add it to the `toctree` directive of that chapter.

For instance, if you added a new page called `hosting-jams-on-aws-guide.md` to the Jami user manual in the `user` folder, you should add it in the `toctree` directive of `user/index.md`, *without* the file extension:

<pre>
```{toctree}
:maxdepth: 1

bug-report-guide
hosting-jams-on-aws-guide
```
</pre>