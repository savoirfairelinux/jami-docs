Release process
===============

Each Jami sub-project has its own repository, build process, integration
cycle and so on. More over the **Jami architecture is split into two
independent modules**: LibRing *(daemon)* and clients.

Having a unique revision is not a solution in this situation. The
retained idea is having a global "state" and **various updates per
module**.

For consistency, **each Jami module has to follow the same process** as
described in following points. But not all modules have to be modified
in same time.

------------------------------------------------------------------------

**PROCESS FLOW:**

1 | 2 | 3 | 4 | 5 | 6
:-----:|:-----:|:-----:|:-----:|:-----:|:-----:
Redmine Ticket |Repository Preparation |Testing |Push tags |Packaging | Advertisement

------------------------------------------------------------------------

Redmine Ticket
--------------

Create a new Task on redmine attached to the release story, for the
right sub-module. Set the title to "Release Major.Minor.Micro", with the
appropriate version number.

Repository Preparation
----------------------

**This section was outdated and removed**

Testing
-------

* Remove any existing Jami installations from your machine.
* Start with clean git tree by running `git clean -d -f -x` from the top
level directory of the project.
* Build and install the daemon and client, see How\\\_to\\\_build
* Run the test suite in daemon and client, on different distributions and
machines.
* Run manual tests
    * Try registering and using different accounts.
    * Try making calls between Jami and other free softphones (Ekiga,
Linphone), as well as hardware VoIP phones.
    * To catch uninitialized values being used, memory leaks, invalid frees,
etc. run `valgrind --track-origins=yes --db-attach=yes ./bin/dring`

Push tags
--------

`git push --tags`

Packaging
---------

cf [This page](/contribute#Packaging)