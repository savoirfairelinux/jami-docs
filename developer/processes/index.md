# Processes

This section describes the processes that are used to develop and release the software.

```{toctree}
:maxdepth: 1

design-process
release-process
```