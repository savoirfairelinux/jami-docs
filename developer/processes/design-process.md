Design Process
==============

## Definitions

+ Client: The person who is paying for the feature (and can be the PO because it's a R&D project)

## Process

1. Ideas:
    + The **client** come with a new Idea
2. Ask for justifications:
    + Context
    + Who (user definitions)
    + Why do we want this feature
    + When do we want this feature
    + Where do we want this feature (platforms)
    + Priority
3. Decide if we want or not this feature, Yes/No
4. If yes, Brainstorming
    + Should be the max of person (users/tech/**client**)
5. Produce a brief of the feature (User Story, Details from step 2, Notes from Step 4.) => Meta ticket on GitLab
6. Create UX Wireframe
7. Validation (user/technical/**client**) Yes/No
8. If no, return to step 6.
9. If validated, POC (Adobe XD/Figma)
10. Validation (user/technical/**client**) Yes/No
11. If no, return to step 9.
12. Create sub-tickets (GitLab) with design specification, resources evaluation (time/resource)
13. Prioritize or stop the process (too much time needed)
14. Develop
15. Validate implementation (Design + **client**) - YES/No
16. If no return to 12.
17. If validated merge and release!

