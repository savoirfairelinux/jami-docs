# Working with Gerrit

## Account setup

* Gerrit server: <https://review.jami.net>
* User documentation: <https://review.jami.net/Documentation/intro-user.html>
* Jami projects on Gerrit: <https://review.jami.net/admin/repos/>

1. Sign in with your Google, GitHub, or git.jami.net account.
2. You'll also need to [upload an SSH key](https://review.jami.net/settings/#SSHKeys) to be able to commit changes for review.
3. Don't forget to select a username.
4. Finally, the email address specified in your git config must match one of the email addresses registered with your Gerrit account.

```{important}
For Savoir-faire Linux Inc. employees: please continue to use your @savoirfairelinux.com email address.
```

### To view your Git config

`git config --list`

### To test your SSH access

To check that your SSH access is properly set up, run the following command:

`ssh -p 29420 <username>@review.jami.net`

<username> is your Gerrit username that you should have set during the account creation. If not, you can do that here.

If your access is granted, you should see a message like:

```bash
****    Welcome to Gerrit Code Review    ****

Hi, you have successfully connected over SSH.

  Unfortunately, interactive shells are disabled.
  To clone a hosted Git repository, use:

  git clone ssh://<username>@review.jami.net:29420/REPOSITORY_NAME.git

Connection to review.jami.net closed.
```

## Git configuration

Gerrit is the official Git repository.

### To update the configuration

You must update your remote information to now use the Gerrit repository.
To do that, update your origin URL:

`git remote set-url origin ssh://<username>@review.jami.net:29420/<project_name>`

Replace `<project_name>` with the correct project (example: jami-daemon).

Or clone the existing repository if you want to start fresh.

## To Push by Default in refs/for/master

You can configure Git to automatically create a review when a change is pushed.

`git config remote.origin.push HEAD:refs/for/master`

## To Create the Review

When pushing to this magic branch, a review will automatically be created on Gerrit.

`git push origin HEAD:refs/for/master`

If you configured the default to refs/for/master as described above, simply

`git push`

If HEAD currently points to the branch with the commits you'd like to push.
Ideally, you should work in a feature/bug branch for the issue at hand. Then you can do:

`git push origin <bugfix_branchname>:refs/for/master`

If this is the first time you've pushed, you will be prompted to install a post-commit hook to insert a Change-ID in your commit message. Gerrit needs this to track patch sets and will reject pushes until you install it.
Simply copy and paste the command to install the hook as instructed by Gerrit and amend your commits.

## To push a private patch

You can push a work in progress (a.k.a. draft) by pushing to `refs/for/master%private`.

For instance, you may want a "private" remote to push to; open <project_dir>/.git/config and add:

```
[remote "private"]

    url = ssh://<username>@review.jami.net:29420/jami-daemon
    push = HEAD:refs/for/master%private
```

Then:

`git push private`

Private work the same way as patchsets, except they are not visible to others by default and don't trigger any Jenkins builds.
A draft can then be shared or published.