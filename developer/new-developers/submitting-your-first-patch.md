# Submitting your first patch

```{seealso}
Set up Git and SSH to work smoothly with Gerrit.
First see [Creating a gerrit review](working-with-gerrit).
```

### SSH setup

<https://review.jami.net/Documentation/user-upload.html#ssh>

1. Generate a personal, dedicated public/private key set.
```bash
ssh-keygen -f ~/.ssh/jami_gerrit_review`
```

Your identification has been saved in `jami_gerrit_review`.
Your public key has been saved in `jami_gerrit_review.pub`.

2. Tell Gerrit your public key.
   1. Log in to [Gerrit](https://review.jami.net) via your GitLab account (Sign-in → OAuth GitLab).
   2. Follow the SSH key instructions found in (your) user options [settings](https://review.jami.net/settings/).
3. Set up your local SSH (via `ssh-add` or in `~/.ssh/config`).
4. Test all of the above (SSH on the service or just try to clone a repo on Gerrit via SSH).

### Your repository

```{important}
This *assumes* you have moved any existing project clone out of the way.
```

1. Clone a (fresh) copy of the project, `git clone ssh://USERNAME@review.jami.net:29420/PROJECT_NAME_GOES_HERE.git`; e.g., `git clone ssh://foo@review.jami.net:29420/jami-project.git`.
2. Configure this clone's local .git/config (optional).
3. Generate commit Change-Ids.
   Also known as commit-msg hook script.

A shell script to be installed to .git/hooks/commit-msg.

The script creates a unique Change_Id:hash for each commit made from your repository.

* The first time you attempt a _push_ to review `git push origin HEAD:refs/for/master`, Gerrit will notice a lack of Change-Id.

* Gerrit will also present a secure copy (scp) string.
  Use *that* to download a copy of .git/hooks/commit-msg.

## References

* [Some Gritty Details](https://review.jami.net/Documentation/user-upload.html#_gritty_details)