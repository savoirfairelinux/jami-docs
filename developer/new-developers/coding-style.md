# Coding style

**This page provides rules and/or guidance for developers wanting to integrate code into Jami.**

C++ format rules are defined by the following clang-format file:
<https://git.jami.net/savoirfairelinux/jami-daemon/blob/master/.clang-format>.

QML format rules are defined by the source code used to build the qmlformat executable:
<https://codebrowser.dev/qt6/qtdeclarative/tools/qmlformat/qmlformat.cpp.html>.

It is recommended that developers format their code using the `jami-project/scripts/format.sh` script.
This is done automatically (as a pre-commit hook) when using `./build.py --init --qt=<path-to-qt>`.
