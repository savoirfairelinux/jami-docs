The APIs of Jami
================

## OpenDHT

The documentation related to the API of OpenDHT is [here](https://github.com/savoirfairelinux/opendht/wiki/API-Overview) and will not be detailed in the following part.

## Daemon

### The managers

The API of the daemon is decomposed between 5 Managers + 1 Instance file:
+ The **CallManager** interface is used to manage call and conference related actions. Since the Jami daemon supports multiple incoming/outgoing calls, any actions involving a specific call must address the method by the means of a unique callID.  Jami daemon will generate a unique callID for outgoing and incoming calls.
+ The **ConfigurationManager** used to handle the configuration stuff: accounts settings, user preferences, ...
+ The **PresenceManager** is used to track the presence of contacts
+ The **VideoManager** used to manage video devices and renderers
+ The **Instance** is used to count the number of clients actually registered to the core. When initializing your client, you need to register it against the core by using this interface.

### DBUS

All the documentation and code for the dbus API is located in `jami-daemon/bin/dbus`.

If you use linux, you can use `d-feet` when the daemon is running to manipulate the API (or with any another tool).

The LRC project uses this API (and use libwrap on windows and mac os).

### JNI

All the documentation and code for the JNI API is located in `jami-daemon/bin/jni`.

### node js

All the documentation and code for the Node JS API is located in `jami-daemon/bin/nodejs`. This API is not used in any known project and maybe is not up-to-date.

### REST

All the documentation and code for the REST API is located in `jami-daemon/bin/restcpp`. This API is not used in any known project and maybe is not up-to-date.

### Python wrapper

A Python wrapper is available in `jami-daemon/tools/jamictrl`. This wrapper uses DBus.

## Plugins

Plugins are a good way to customize Jami if you want to interact with streams (Audio, Video or chat). Several plugins are already available
through the Plugin Store (in the settings of your Jami application).