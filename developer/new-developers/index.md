# New developers

This part of the documentation is intended for new developers who want to contribute to the project.
It explains some of the tools and conventions.

```{toctree}
:maxdepth: 1

apis-of-jami
coding-style
debugging-tools
improving-quality-of-jami
qt-qml-coding-style
qt-qml-testing-tools
submitting-your-first-patch
working-with-gerrit
```