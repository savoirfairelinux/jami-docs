# Debugging tools

There are several ways to debug Jami from a developer perspective, depending on what is required to be debugged.

## Loggers

The first way is to use runtime loggers.
Starting `jami` with `-d` will enable logging by the daemon (or the Troubleshoot section in the General settings).
All logs are not enabled by default, as Jami uses several libraries.
However, passing environment variables enables logs:

+ `SIPLOGLEVEL=5` enables logs from PJSIP.
+ `DHTLOGLEVEL=5` enables logs from OpenDHT.
+ `AVLOGLEVEL=50` enables logs from ffmpeg.

## Debuggers

Generally, the IDE has an embedded debugger.
Otherwise, `gdb` can be used, for example, to be able to add breakpoints, backtraces from crashes, print internal structures, etc.
To get debug symbols, it is required to compile the project in *DEBUG* mode.

Some useful commands:

+ `b file.cpp:line` - add a breakpoint (*file.cpp:line* can be replaced by a symbol)
+ `t a a bt` - (thread apply all backtrace) to get all backtraces
+ `Ctrl + X / A` - pass in graphical view
+ `p` - print an internal value.

```{note}
Visual Studio Code is fully supported by Jami and can be used to debug the project.
```

## Profilers

Debuggers are useful, but they do not show real-time memory consumption/network activity/CPU usage.
For this, an embedded profiler in the (Android Studio, Qt Creator/Visual Studio, etc.) IDE can be used.

## AddressSanitizer

[AddressSanitizer](https://github.com/google/sanitizers/wiki/AddressSanitizer) can be useful to detect leaks, crashes, and potential deadlocks at runtime.
To enable this, compile the daemon with `CXXFLAGS+="-fsanitize=address"`.
Other flags like `tsan` may be useful.

## Valgrind/Callgrind

[Valgrind](https://valgrind.org/) is a tool to watch allocations, CPU usage, and more and can be used via:
`valgrind --tool=callgrind ./jami -d`.
This will make the application very slow but can provide useful reports about memory allocation/performance usage (**KCacheGrind** can be used to read reports).

## Tests

Daemon has many tests and coverage enabled.
If the daemon is built in static (else private symbols will not be available), adding new tests can help to reproduce bugs, solve bugs, and avoid any regression.
(cf. `daemon/tests/unitTests`)

## Agent

Tests are only using one daemon to simulate both peers.
So it can be difficult to test in various environments.
Another possibility is to write a scenario and run an agent (documentation is available in the daemon's repository).

## LTTng

Finally, tracepoints can be created and analyzed.
`daemon/tools/trace` provide the documentation and some examples.
The advantage of [LTTng](https://lttng.org/) is that it is quicker than logs, can be triggered by system events and can be used with tracepoints already present in the kernel (so that it can be used with tracepoints from network interfaces).

## Tests

Both clients and daemon have tests.
Daemon's tests are written in C++ and use the `cppunit` framework.
They are located in the `daemon/tests` directory.