Setting up your own TURN server
===============================

Jami can be configured to use TURN or STUN servers ([RFC
5766](https://tools.ietf.org/html/rfc5766)) to establish a connection
between two peers.

The default TURN server is "turn.jami.net", with username "ring",
password "ring", and realm "ring".

In this guide, we will setup a
[coturn](https://github.com/coturn/coturn) server. There are other
TURN/STUN server implementations available under a free license, such
as [TurnServer](http://turnserver.sourceforge.net/) and
[Restund](http://www.creytiv.com/restund.html).

## Installing

COTURN is available in most Linux distributions. On Debian, install it
with the following command:

```bash
apt-get install coturn
```
## Configuring

Here is a basic `turnserver.conf` file:

```
listening-port=10000
listening-ip=0.0.0.0
min-port=10000
max-port=30000
lt-cred-mech
realm=sfl
```

This also will function as a STUN server. The STUN server does not
require a username and password (STUN uses very little bandwidth).

## Creating users on your TURN server

To create users on your TURN server, use the `turnadmin` binary (this
might require superuser permissions).

```bash
turnadmin -a -u bob -p secretpassword -r sfl
```

## Launching the TURN server

```bash
turnserver -c turnserver.conf
```

## Configuring Jami to authenticate with the TURN server

You can configure Jami to use your TURN server from the advanced
section of your account settings:

| Field          | Value                        | Example        |
|:---------------|:----------------------------:|:--------------:|
| **server url** | host and port of your server | 0.0.0.0:10000  |
| **username**   | username                     | bob            |
| **password**   | password                     | secretpassword |
| **realm**      | realm                        | sfl            |
