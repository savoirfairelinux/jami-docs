Location Sharing on QT Client
=============

How to use it?
--------------

### Trivial use

In a conversation, the user can click on the location icon to display a map. If `Location Services` is enabled on the device, the user's position will be shown on the map, along with the locations of all other members who are sharing their location (from all conversations of the selected account). The user can toggle location sharing on and off by clicking the location sharing buttons. By default, the user's location is shared for 15 minutes, but this setting can be modified in the app's settings.

When a user is sharing their location in a conversation, a red location icon will appear on the conversation icon. If the user receives a location from another member, an orange location icon will be displayed on all conversations in which the member is present.

The user can interact with the map by recentering, zooming in and out, moving it, and closing it. The map can also be pinned or unpinned. When the map is unpinned, it can be repinned if the user is in the right account. This feature allows the user to keep the map visible while continuing to use Jami.

### Advanced use

#### Multi-sharing

##### scenario

The user is already sharing location with conversation A and conversation B.

##### Feature

If the user switches to conversation C, the map is still visible and the user has two options

1. Click on the location sharing button to also share location with the members of conversation C.
2. Click the location sharing end button. This will bring up a pop-up that allows the user to either turn off location sharing entirely or only stop sharing their location with the members of conversation C.

#### Pinned window

If the window is unpinned, the user will not be able to click on the location sharing button because the window is no longer connected to a conversation. To start sharing location with new people, the user must re-pin the window. Note that unpinning the window does not affect any ongoing location sharing. Additionally, clicking on the location sharing end button while the window is unpinned will stop all current location sharing and no pop-up will appear to allow the user to select a specific conversation to stop sharing with.

#### Multi accounts

Each account has its own unique map. If the user unpins the map while on account A, then switches to account B and unpins the map, two maps will be visible. The maps for accounts A and B display positions shared with those accounts, respectively, and are completely separate from each other


How it works? (technical)
-------------------------

### Introduction

This feature is divided into three parts:

1. Sending one's position
2. Receiving a position
3. Displaying a position

To determine the location, [Qt Positioning](https://doc.qt.io/qt-6/qtpositioning-index.html) API is used. Once the position is determined, it is sent as a message on the DHT and is transmitted to the client.
The received position is then displayed using the [OpenLayers](https://openlayers.org/) JavaScript library.

#### Sending a position

As soon as a map is opened, the `Positioning` class takes care of retrieving the current position using the `QGeoPositionInfoSource` class of the `QtPositioning` module.
The position is then converted to JSON format and is transmitted to the `positionManager`.
This class coordinates the entire position sharing process.
The position is then shared through the `sendPosition()` function.
It is shared:

+ Locally through the `localPositionReceived` signal so that the user can see their own position

+ On the DHT to all conversations in the `positionShareConvIds_` list. This list contains the keys of all conversations that the user wants to share their position with. From this key, the URIs of all participants are obtained and a position message is sent to each participant.

The JSON format of a position is as follows:

+ Type (position or stop message)
+ Latitude
+ Longitude
+ Time (unused by the QtClient)

An example of data: `{\"lat\":45.51616583988481,\"long\":-73.620693,\"time\":1671658862000,\"type\":\"Position\"}`

When sending the position to the daemon, the author's URI is also transmitted.

When the user stops sharing their position with a conversation, the conversation's ID is simply removed from the `positionShareConvIds_` list. A "stop" message is also sent to each participant.

### Receiving a position

When a position is received, it triggers the 'onPositionReceived()' slot. Whether it is a local position from the `QtPositioning` module or a position from another contact.
At the `positionManager` level, the `objectListSharingUris_` list stores all of the client's positions. The position is either:

+ Added (the URI is not present in the list)
+ Updated (the URI is already present in the list)
+ Deleted (type = "Stop")

The position is stored in the list in the form of an object of type `positionObject`. This type allows for a watchdog for each position. If the position is not updated within a certain time frame, it is removed from the list.

### Displaying a position

When a position is received (slot `onPositionReceived()` triggered), the position is transmitted to Qml which in turn sends the information to the [OpenLayers](https://openlayers.org/) JavaScript library. The Qt `WebEngine` module allows for the bridge between the library's web technology and Qml. Each position is represented by a layer added to the map. The `newPosition()` function adds a new layer, the `updatePosition()` function updates the coordinates of the layer, and the `removePosition()` function removes the layer.


