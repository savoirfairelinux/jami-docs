# Going further

This part of the documentation is dedicated to developers who want to dig into some more specific topics about Jami.

```{toctree}
:maxdepth: 1

choosing-crf-value-for-encoder
delivery-status
important-rfcs
location-sharing
message-displayed-status
setting-up-your-own-turn-server
```