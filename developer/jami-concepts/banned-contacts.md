Banned contacts
===============

Following information are here for development purposes and may not
reflect the current state of any Jami client.

Introducing scenario
--------------------

Let's explain banned contacts with a simple scenario:

Alice and Jessica are friends, and like all good friends do, they use
Jami to communicate. They are both Jami contact of each other, so Alice
is a contact of Jessica and Jessica is a contact of Alice. Some day
however, Jessica does something really bad to Alice and Alice doesn't
want to hear from her anymore. Instead of removing Jessica from her
contacts -- which would still allow Jessica to send her contact
requests, Alice *bans* Jessica.

**So, what does it mean?**

In the daemon
-------------

Jessica *won't be notified that she was banned by Alice*.

As a *banned contact* of Alice, Jessica *won't be allowed to contact her
anymore*, in any way. Text messages, voice or video calls won't be
acknowledged or answered in any way. Alice won't even be aware that
Jessica tried to contact her.

Banned contacts are synched across linked devices like other contacts.

In Jami clients (recommended implementation)
--------------------------------------------

As long as Jessica is a banned contact, the conversation with Jessica
doesn't appears in the conversations list. The conversation history,
however, is not deleted. Jessica appears in Alice' account banned
contact list. Alice might also find/open the conversation by performing
an exact search for Jessica' username.

Alice can un-ban Jessica from the conversation or the banned contact
list.

The search result and the conversation indicates Jessica' banned status.
Alice must unban Jessica to send her a message, call her or otherwise
interract with her.

Alice can still delete the conversation history using a *Delete History*
button.
