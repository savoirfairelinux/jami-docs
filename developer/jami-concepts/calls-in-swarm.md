Calls in Swarm
==============

## Goals

This page aims to describe how call will works with groups. This method will allow clients to provides call for multiple participants with a shared text history. The protocol must be flexible, keep compatibility with the SIP stack, adn must work in a distributed environment. This distributed environment also brings some challenges, such as possible race conditions (where multiple parties can start a call at the same time). Finally, because the Swarm can be with members only using mobiles, or by companies with dedicated servers to mix the video, it should also be able to work with the best experience possible on heterogeneous environment.

## Definitions

+ a **Rendezvous** will mix all authorized incoming calls in a conference.
+ Two new URIs will be used for this feature:
    + `swarm:xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx`
    + `rdv:accountUri/deviceId/conversationId/confId`
+ **Swarm's profile** is the profile of the swarm stored in a vCard (`profile.vcf`, see {doc}`Conversation's profile synchronization <swarm>` and {doc}`synchronizing-profiles`)
    + `rdvUri` is the default URI for a Swarm, set in the *Swarm's profile*

Note: those two URIs allow the client to place a call in order to join a *Rendezvous* or to init a call to a swarm, resulting in the logic described in the next section.

## Place a call in a Swarm

Proposed flow:

+ **a:** If `rdvUri` is set:
    + Call it
        + If failed, go to **b:**
+ **b:** else if there is a current active call:
    + Call it
        + If failed, go to **c:**
+ **c:** else, host a new *Rendezvous* and join it.
    + Also, ask users if they want to be the host for next calls.

![Diagram: Placing a call in a Swarm](images/placing-call-in-swarm-diagram.png)

For each calls, two commits are added by the host of the call in the swarm. A first one to announce that a new call is starting and give information about who is hosting and the conference id. And a second to announce that the call is finished (so other peers can compute how many calls are currently active) and to get the duration of the call to show.

## Incoming calls for swarm

When receiving a new incoming call, the URI MUST be parsed. If the incoming call is for a *Rendezvous* which is currently hosted by the device and the caller is authorized to join, the call MUST be answered and added to the current *Rendezvous*

## Defining a host for a Swarm

To define a default URI, the member who wants to be the host should update the profile of the conversation, and this will be synced and validated by peers. However, the host MUST be able to accept or not to be the host and a member with enough permissions SHOULD be able to ask a device to be the host. This process is not decided for now.

For now, only the administrator can add one of its device as the default host.

## Future

### Join notifications?

Members of the swarm that aren't in a call SHOULD be able to know who is in the active calls.

### SIP Bridge?

Because conferences are still mixed SIP calls, a SIP bridge is still possible. In the *Swarm's profile* it should be possible to add a SIP entry, like some popular VoIP systems)

### Set another member's device as a host