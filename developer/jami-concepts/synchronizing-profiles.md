# Profile synchronization

There is two types of profiles. The one for the current account, and the ones from contacts. As this vcard contains infos, such as the avatar and the display name, it should be synced as soon as possible. But, it must be sent only when needed, to avoid un-necessary data transmissions. The format used to store the profiles is a [vCard](https://en.wikipedia.org/wiki/VCard) (https://datatracker.ietf.org/doc/html/rfc6350)

## Where are stored profiles?

### Self

It's in `${idPath_}/profile.vcf` e.g.: `.local/share/jami/xxxxxx/profile.vcf` on GNU/Linux.

### Contacts

Because this logic is still managed per client, it depends on the client. However, on Desktop, this is unified and stored into:
`${idPath_}/profiles/${base64(uri)}.vcf`

## When a profile is sent


### Calls

At the beginning of a call, generally client sends the vCard. However, this method should be removed in favor of the other located in the daemon.

### Send to contact's devices

When connecting to a new devices, a SIP or a sync channel is done. When this channel is ready, we check if we need to send the profile. To do this, the hash of the profile is compared to the one in the cache (`${cacheDir}/${accId}/vcard/sha3` like `~/.cache/jami/xxxxx/vcard/sha3`on GNU/Linux), and the file `${cacheDir}/${accId}/vcard/deviceId` is checked). cf. `JamiAccount::needToSendProfile()`. If not present, the profile of the account is sent. Typically, the name of the channel will be "vcard://xxx" where the issuer is the one sending the profile.

### Multidevices

If the profile is sent and the device is from the same account, conversations are checked and we send profiles of all members. The channel name will be "${conversationId}/profile/${memberUri}.vcf". Behavior tested in `daemon/test/unitTest/syncHistory/syncHistory.cpp`
