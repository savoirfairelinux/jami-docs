Account management
==================

In this part, we will learn how to manage a Jami account. This means, how to create a Jami account, modify the basic settings and delete the account. This part will *NOT* explain what all the settings mean or how we can use the account to do any action like adding a contact.

## What is an account

In Jami, an account is a X509 certificate chain generally containing 3 certificates:
1. CA (Self-Signed if locally generated or a company)
2. Account (Where the fingerprint of the public key is called the "Jami ID")
3. Device

This allow a company to revoke an account if needed and an account to revoke a device (if one get stolen for example).

## Create a new account

### Daemon side

#### API

In cx.ring.Ring.ConfigurationManager:

```xml
<method name="addAccount" tp:name-for-bindings="addAccount">
    <tp:docstring>
        Add a new account. When created, the signal <tp:member-ref>accountsChanged</tp:member-ref> is emitted. The clients must then call <tp:member-ref>getAccountList</tp:member-ref> to update their internal data structure.
        <tp:rationale>If no details are specified, the default parameters are used.</tp:rationale>
        <tp:rationale>The core tries to register the account as soon it is created.</tp:rationale>
    </tp:docstring>
    <annotation name="org.qtproject.QtDBus.QtTypeName.In0" value="MapStringString"/>
    <arg type="a{ss}" name="details" direction="in"  tp:type="String_String_Map">
        <tp:docstring>
             The new account settings
        </tp:docstring>
    </arg>
    <arg type="s" name="createdAccountId" direction="out">
        <tp:docstring>
             A new account ID
        </tp:docstring>
    </arg>
</method>
```

The details can be retrieved from the method `getAccountTemplate(type)` with `type=JAMI` or `type=SIP`. For example, this is the following code used in LRC.

```cpp
std::string
NewAccountModel::createNewAccount(profile::Type type,
                                  const std::string& displayName,
                                  const std::string& archivePath,
                                  const std::string& password,
                                  const std::string& pin)
{

    MapStringString details = type == profile::Type::SIP?
                              ConfigurationManager::instance().getAccountTemplate("SIP") :
                              ConfigurationManager::instance().getAccountTemplate("JAMI");
    using namespace libjami::Account;
    details[ConfProperties::TYPE] = type == profile::Type::SIP? "SIP" : "JAMI";
    details[ConfProperties::DISPLAYNAME] = displayName.c_str();
    details[ConfProperties::ALIAS] = displayName.c_str();
    details[ConfProperties::UPNP_ENABLED] = "true";
    details[ConfProperties::ARCHIVE_PASSWORD] = password.c_str();
    details[ConfProperties::ARCHIVE_PIN] = pin.c_str();
    details[ConfProperties::ARCHIVE_PATH] = archivePath.c_str();
    QString accountId = ConfigurationManager::instance().addAccount(details);
    return accountId.toStdString();
}
```

When a new account is added, the signal `accountsChanged` will be emitted. The client should update its internal structure after this signal with other methods in ConfigurationManager.

#### Core

The main logic to create a new account is located in `src/ringdht/ringaccount.cpp`, in `RingAccount::createAccount`

### How it works, from scratch

A Jami account is in fact represented by some files stored in a gzip archive. If a password is provided during the account creation, the archive will be encrypted as following: `dht::crypto::aesEncrypt(archive, password)` (`dht::crypto::aesEncrypt` is defined in OpenDHT and use `nettle/{aes,gcm}`). This is what the archive will contain a big JSON file with:

1. The private key `ringAccountKey` and certificate chain `ringAccountCert` (base64 encoded)
2. Generated CA key (for self-signed certificates) `ringCAKey`
3. Revocated devices `ringAccountCRL`
4. The ethereum private key `ethKey` for the device. It's only used when you register your name on `ns.jami.net`. Not mandatory.
5. The contacts
6. The account settings

So let's generate it!

**TODO**


## Delete the account

Deleting a Jami account is pretty simple. Because the keys are only on the device, if the keys are deleted... the account is deleted! The only thing outside the device is the username, on the nameserver. To remove this info, it depends how the nameserver work. For example, it's not possible with https://ns.jami.net


### Daemon side

#### API

In cx.ring.Ring.ConfigurationManager:

```xml
<method name="removeAccount" tp:name-for-bindings="removeAccount">
    <tp:docstring>
        Remove an existing account. When removed, the signal <tp:member-ref>accountsChanged</tp:member-ref> is emitted. The clients must then call <tp:member-ref>getAccountList</tp:member-ref> to update their internal data structure.
    </tp:docstring>
    <arg type="s" name="accoundID" direction="in">
        <tp:docstring>
             The account to remove, identified by its ID
        </tp:docstring>
    </arg>
</method>
```

When the account is deleted, the signal `accountsChanged` will be emitted. The client should update its internal structure after this signal with other methods in ConfigurationManager.

#### Core

The main logic to create a new account is located in `src/manager.cpp`, in `Manager::removeAccount`. It removes the accounts files and update the config (`dring.yml`).

## Update the details of an account

### API

In cx.ring.Ring.ConfigurationManager:

```xml
<method name="setAccountDetails" tp:name-for-bindings="setAccountDetails">
    <tp:docstring>
        Send new account parameters, or account parameters changes, to the core. The hash table is not required to be complete, only the updated parameters may be specified.
        <tp:rationale>Account settings are written to the configuration file when the app properly quits.</tp:rationale>
        <tp:rationale>After calling this method, the core will emit the signal <tp:member-ref>accountDetailsChanged</tp:member-ref> with the updated data. The client must subscribe to this signal and use it to update its internal data structure.</tp:rationale>
    </tp:docstring>
    <arg type="s" name="accountID" direction="in">
    </arg>
    <annotation name="org.qtproject.QtDBus.QtTypeName.In1" value="MapStringString"/>
    <arg type="a{ss}" name="details" direction="in" tp:type="String_String_Map">
    </arg>
</method>
```

The map can contains a partial update and `accountDetailsChanged` will be emitted on success. `getAccountDetails`

## Add a device

There is two possibilities to add a device.

### Backup archive (Then import from backup)

#### API

In cx.ring.Ring.ConfigurationManager:
```xml
<method name="exportToFile" tp:name-for-bindings="exportToFile">
    <tp:added version="5.1.0"/>
    <tp:docstring>
        Copy the account archive to the path provided in argument.
    </tp:docstring>
    <arg type="s" name="accountID" direction="in">
    </arg>
    <arg type="s" name="destinationPath" direction="in">
    </arg>
    <arg type="s" name="password" direction="in">
    </arg>
    <arg type="b" name="success" direction="out">
        <tp:docstring>
            True if the operation was initialized successfully.
        </tp:docstring>
    </arg>
</method>
```

### Export on DHT

#### API

In cx.ring.Ring.ConfigurationManager:
```xml
<method name="exportOnRing" tp:name-for-bindings="exportOnRing">
    <tp:docstring>
        Export account on the DHT using the given password and generated PIN (returned through exportOnRingEnded signal).
    </tp:docstring>
    <arg type="s" name="accountID" direction="in">
    </arg>
    <arg type="s" name="password" direction="in">
    </arg>
    <arg type="b" name="success" direction="out">
        <tp:docstring>
            True if the operation was initialized successfully. exportOnRingEnded will be trigered on completion.
        </tp:docstring>
    </arg>
</method>
```

Then `exportOnRingEnded` is emitted.

## Revoke device

### API

```xml
<method name="revokeDevice" tp:name-for-bindings="revokeDevice">
    <tp:docstring>
        Revoke device attached to the given Jami account, and publish the new revocation list.
    </tp:docstring>
    <arg type="s" name="accountID" direction="in">
    </arg>
    <arg type="s" name="password" direction="in">
    </arg>
    <arg type="s" name="deviceId" direction="in">
    </arg>
    <arg type="b" name="success" direction="out">
        <tp:docstring>
            True if the operation was performed successfully.
        </tp:docstring>
    </arg>
</method>
<signal name="deviceRevocationEnded" tp:name-for-bindings="deviceRevocationEnded">
    <tp:docstring>
        Notify clients when the revokeDevice operation ended.
    </tp:docstring>
    <arg type="s" name="accountID">
    </arg>
    <arg type="s" name="deviceId">
    </arg>
    <arg type="i" name="status">
        <tp:docstring>
            Status code: 0 for success
            <ul>
                <li>SUCCESS = 0         everything went fine. Device is now revoked.</li>
                <li>WRONG_PASSWORD = 1  revocation failed: wrong password.</li>
                <li>UNKNOWN_DEVICE = 2  revocation failed: unknown device.</li>
            </ul>
        </tp:docstring>
    </arg>
</signal>
```