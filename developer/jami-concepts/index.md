# Jami Concepts

Jami is a quite complex platform interacting with many different components and introducing many new concepts.
This manual is intended to help you understand how Jami works, how to develop for Jami, and how to contribute to the project.

To quickly summarize and help you to find the part of the manual that you are looking for, here is a brief overview of the different sections:

Jami is a distributed platform that allows users to communicate without the need for a central server.
So, a Jami account consists of a chain of certificates (CA, account, and device) that is used to authenticate the user and to encrypt the communication.
The account is stored on the user's device, not on a server.

For this, here is the page that explains how it works:
* [account-management](account-management.md)
* [jami-identifiers](jami-identifiers.md)

After the user account is created, the user can contact other people by sending connection requests through a DHT (<https://opendht.net>) after finding the contact ID
(directly known or by looking up the name server).
If both parties agree, they will first try to create a P2P TCP link (with the ICE protocol), then encrypt it via TLS and use it as a multiplexed socket to transmit data for various protocols, cf.:

* [contact-management](contact-management.md)
* [banned-contacts](banned-contacts.md)
* [connection-manager](connection-manager.md)
* [name-server-protocol](name-server-protocol.md)

When both peers are in their contact list, a conversation is created.
This conversation is a **swarm** based on **Swarm Technology**.
A **swarm** is synced between all devices in a conversation via the `git` protocol (it's a Git repository), and connections across devices in a conversation are routed by a component called the **DRT** (distributed routing table).

* [swarm](swarm.md)
* [drt](drt.md)

Then, the connection can be used to send messages, files, or to make calls (1:1 or conferences).

* [calls](calls.md)
* [calls-in-swarm](calls-in-swarm.md)
* [file-transfer](file-transfer.md)
* [conference-protocol](conference-protocol.md)

```{note}
   Only calls using Jami accounts are based on Swarm Technology.
```

Finally, a user can have multiple devices, and a lot of information is synced between them.

* [synchronizing-profiles](synchronizing-profiles.md)
* [synchronization-protocol](synchronization-protocol.md)

```{toctree}
:maxdepth: 1

account-management
banned-contacts
calls-in-swarm
calls
conference-protocol
connection-manager
contact-management
drt
file-transfer
jami-identifiers
name-server-protocol
swarm
synchronization-protocol
synchronizing-profiles
```