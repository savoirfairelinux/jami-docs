# Developer manual

The Jami developer manual is a reference for Jami developers and contributors.
It documents the various aspects of hacking on and developing Jami.
It includes in-depth explanations of how Jami is designed and how its various parts work together.

```{toctree}
:maxdepth: 1

feature-requests
new-developers/index
jami-concepts/index
going-further/index
processes/index
```