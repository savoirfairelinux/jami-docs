# Feature requests

This page exists to classify feature requests per users' feedback, to avoid leaving tickets open for years.
Under construction, tickets require sorting.


## SFL

+ Connectivity → Contact #Jami (Francois-Simon) / medium:
    + TURN IPv6 support
    + connection IPv6 only↔IPv4 only
+ SIP:
  + Fix IP to IP → Contact #Jami (Adrien, Francois-Simon) / easy
  + Add unit-tests / probably easy
  + Search in system contacts ({client-gnome-issue}`1191`,
    {client-gnome-issue}`829`, etc.) → Contact #Jami (Adrien,
    Francois-Simon, Andreas, etc.) / easy but long
+ Crypto: (Contact #Jami - Adrien)
  + Update media RTP ciphers for ffmpeg / probably easy
  + Support ECC (instead RSA) for Jami accounts / hard but short
  + Remove usages of SHA1 (ids + git) / hard
+ Media → Contact #Jami (Adrien, Andreas, etc.)
  + {daemon-issue}`POC for AV1 support <27>` / probably hard
  + Investigate audio quality (contact #Jami, medium difficulty)
+ Plugins ideas: (Contact #Jami - Aline)
  + {plugins-issue}`Payment plug-in <27>`
  + A chat-GPT plugin
+ Add new transports (e.g. QUIC?)
+ Add libtorrent support?
+ Public page/personal blog available through Jami (Contact #Jami
  / medium difficulty & long term project)
+ Qt:
  + {client-qt-issue}`Add support for peer discovery<868>` (contact #Jami - easy)
  + Emoji as a native component to avoid depending on qt-webengine
  for this (not available on macOS) (e.g. <https://github.com/jnodev/QMLemoji>)
  + Categorized conversations (like favorites, groups, etc) (to discuss with #Jami)
+ jami-web:
  + Design architecture to support calls between jami-web and desktop versions
  → Contact #Jami (Adrien, Francois-Simon) / hard

+ Help for the Roadmap 2024: <https://git.jami.net/groups/savoirfairelinux/-/epics/58>

Some other ideas from tickets:


## Planned/In progress

+ Improve Jami on Yocto (some codecs problems/lag on i.MX 6) →
  Contact Rennes
+ [Qt/QML Improve client](https://git.jami.net/savoirfairelinux/jami-client-qt/-/issues)
  → Contact #Jami (Andreas)
    + Implement designs from Charlotte
    + Help for swarms (Francois-Simon)
+ {gerrit-project}`jami-web`; because installing an app can be boring.
  Joining a conference via a link can be cool.  For now, nobody is
  working on it.


## Wanted but not planned

- ?


## Can be implemented, contributions welcome (or will take months/years to come)

+ {project-issue}`add possibility to show "reachable at the following
  times" (contact details) <1261>`
+ {project-issue}`Preferences per contact (allow calls, file transfer,
  custom ringtone) <1280>`
+ {project-issue}`Ability to create polls (ideally a plugin with
  swarm) <1254>`
+ {project-issue}`Support for Panic buttons <623>`
+ Matrix bridge
+ Full TOR support or other alternatives such as lokinet
  ({project-issue}`922`, {project-issue}`622`, {project-issue}`495`, {client-android-issue}`1636`),
  {project-issue}`i2p <630>`
+ {project-issue}`Bluetooth support <774>`
+ {project-issue}`Secret-based turn server <886>`
+ {client-android-issue}`Ability to compress files before sending
  <720>`
+ Ability to trim recorded clips before sending
+ {client-gnome-issue}`Spell checking support <1169>`
+ {project-issue}`Echo bot to test audio <392>`
+ {project-issue}`Handle click on jami:uri system wide (missing macOS/Android/iOS) <653>`
+ {project-issue}`Initial audio mode <1288>`
  (need to wait for group chat)
+ {project-issue}`Feature Request: Volume Slider <1524>`
+ {client-qt-issue}`Portable version for Windows <170>`


## Depends on mass changes

- ?


## Packaging

+ {client-gnome-issue}`FreeBSD support <1272>`


## Others

+ {project-issue}`Collaborative editor <1010>`
+ {project-issue}`A thunderbird plugin <516>`
+ {project-issue}`OpenAlias <928>`
+ {project-issue}`CMIS integration <455>`
+ {project-issue}`Sound safety <441>`
+ {client-gnome-issue}`Ability to see multiple chats at the same time <909>`
+ {client-gnome-issue}`Vocoder option <957>`
+ {project-issue}`SOCKS5 support <430>`
+ {project-issue}`Cardbook integration <383>`
+ {project-issue}`Multiple instances running <629>`
+ {daemon-issue}`Whiteboard <181>`
+ {client-android-issue}`Camera zoom <979#note_29386>`
+ {client-qt-issue}`Emoji reaction in calls <497>`
+ {client-android-issue}`Conversation's background <710>`
+ {project-issue}`[Desktop] Option to have a Window per Chat <633>`
+ {project-issue}`Multiple text selection <1096>`
+ {project-issue}`In-app sticker pack creation <1317>`
+ {project-issue}`Re-order messages <1357>`
+ {daemon-issue}`Remote control <349>`
+ {client-android-issue}`Locked messages (feature that can be enabled to hide messages until they are clicked upon) <1146>`
+ {client-qt-issue}`selecting one or multiple messages and delete them just for the user doing so <1188>`
+ {client-qt-issue}`Drag and drop files from chatview and send to other contacts <485>`
