# Requesting publication

The Extension Store is a system to distribute extension from a server.
The organization needs to be eligible to publish an extension (more details [here](jami-extension-certificate.md)).
To upload an extension, the extension must pass a verification process.

## Extension Store Requests

Create an issue to request upload permissions on the Extensions Store for an organization.
The issue template mechanism is available on [Github](https://github.com/savoirfairelinux/jami-plugins-store-requests).
Two types of requests are possible: requests to authorize a new organization to upload new extensions and requests to upload extensions.

## Authorize a new organization

The organization must deliver a Certificate Signing Request (CSR).
The CSR can be generated with the [SDK](https://review.jami.net/plugins/gitiles/jami-plugins) tool or with the OpenSSL tool.
The certificate authority must review the organization request and sign the certificate with the SDK tool.
The certificate authority must send the certificate to the organization.
Before sending it, a job runs to verify that the certificate is correctly signed.
The certificate authority reserves the right to refuse the certificate request or revoke the certificate.
Upon sending a certificate signing request, the organization agrees to the terms of use of the certificate authority.
If the private key of the organization is compromised, the organization must revoke the certificate and generate a new certificate and notify the certificate authority.

## Upload a new extension

The organization must deliver the extension signed correctly with the certificate of the organization.
A job is triggered to check the signature of the extension and the certificate are correct.
The Jami team is not responsible for the extension content.
