# How it works?

```{note}
This page describes what is a Jami Plugin and how to install and use them.
In the documentation and in the code, we use the term plugin.
However note that the Jami client user interface refers to and displays "plugins" as "extensions".
```

Jami can be break down to three main components that interact together: Daemon, LRC and clients.
Daemon is the core of Jami, and although it does not interact with users, it is involved in every command.
Therefore, Daemon has a `JamiPluginManager` class that among other actions perfoms install/uninstall, load/unload, edit preferences and control plugins' usage.
Despite Daemon importance, what a plugin effectivelly does to a call video/audio or to a chat message is unknown to it the same way Daemon does not know what is effectivelly done by LRC or the clients interfaces.
Plugins then can be seen as a forth interacting component in Jami.

The plugin system inside Jami exposes different APIs that can be used by the plugins.
For instance, the ChatHandler and the Media Handler API.
The latter enables the plugins to modify audio and video streams from Jami calls and is used by our GreenScreen plugin but could also be used to build a YouTube streaming system, various instagram-style filters, a real time translation service, etc.

Plugins can be composed by one or multiple media and chat handlers that are responsible for attaching/detaching a data stream from Jami and a data process.
Each handler represents a functionality that can be totally different between them or can be a modified versions of the same core process.
In our example, we have only one functionality, it being, the GreenScreen plugin has one media handler which data process is responsible for segmenting the foreground from a video frame and applying another image to the background, just like it is done with the green screens in movies!

To use one custom functionality, it is necessary that Jami knows all plugins' handlers, which one is going to be used and the data that should be processed.
Plugin's handlers are created once a plugin is loaded and are shared with Daemon's Plugin Manager.
The data is inside Jami flow (for a call plugin, in the event of a new call, Jami creates and stores the corresponding media stream subjects).
Finally, once a user puts a plugin functionality in action Jami tells this handler to attach the available data.
When deactivated, Jami tells the handler to dettach.

## How to use it?

## Setup
A Jami plugin is a `pluginname.jpl` file, and it must be installed to your Jami.
Once installed, Jami will add your new plugin to the available plugins list but they will not be ready for use yet.
Plugins are libraries and must be loaded if you want to expose them.
Moreover, plugin may have preferences and besides install/uninstall and load/unload actions, it is possible to modify those preferences.
For example, our GreenScreen plugin allows the user to change the background image displayed.

## Android
To setup a plugin for Android you must go under Setting, enable plugins if they are disabled, and select a plugin file from your phone.
After installed it is automaticaly loaded.
Optionally, you can manually perform load/unload using the checkbox button on the plugin list.

![client-android-install](images/jami-extension-client-android-install.gif)

For Android uninstall, you must click on the plugin and a uninstall option will appear allong with the preferences and a reset preferences option.
In order to a preference modification can take effect the plugin has to be reloaded.

![client-android-prefs](images/jami-extension-client-android-prefs.gif)

## Linux/Windows
Similarly, for the client-qt available on GNU/Linux and Windows you must go to Prefences, enable plugins if it is disabled, and select a plugins file from your computer.
Each plugin in the shown list is linked to two buttons beeing:

- Client-qt: a load/unload button and a preferences button;

![client-qt-install](images/jami-extension-client-qt-install.gif)
![client-qt-prefs](images/jami-extension-client-qt-prefs.gif)

## Use!
A media handler functionality only takes place if you turn them on during a call.
For either Android, Linux or Windows you can do so by clicking on the plugins icon on your call screen.

![client-android-call](images/jami-extension-client-android-call.gif)
![client-qt-call](images/jami-extension-client-qt-call.gif)

Similarly, for chat handler functionality, you will see a plugin icon in the chat window as in the images below.