# Publishing on the Extension Store

## Publication on the Extension Store

### Congratulations!

Steps to publish a Jami extension:
1. Jami extension successfully created.
2. Jami extension passed testing.
3. Jami extension signed according to the trust chain of `store_ca > organization > ThePlugin`.
4. Jami extension is now ready to be published.

```{note}
Jami extensions are able to be tested locally.
Only Jami team members are able to publish an extension on the Extension Store.
Contact a Jami team member to publish a Jami Extension.
Jami team members are able to find the server IP address on SFLvault.
```

----------

1. **Publication on jami.net/extensions/:**
Contact the Jami website administrator to make an extension available on [jami.net/extensions](https://jami.net/extensions/).

2. **Distribution for Android:**
Android extensions are compatible on supported Android devices.
The Extension Store is unavailable within the Jami Android client.
However, Jami Extensions for Android can be downloaded from the [jami.net/extensions](https://jami.net/extensions/) website and installed on the Jami Android client.

3. **Distribution for Desktop:**
Desktop extensions are compatible on devices with supported GNU/Linux, macOS, and Windows operating systems.
The Extension Store is available within the Jami Desktop client. This enables users to view and install plugins within the Jami Desktop client.
Additionally, Jami Extensions for Desktop can be downloaded from the [jami.net/extensions](https://jami.net/extensions/) website and installed on the Jami Desktop client.

----------

## How the Extension Store works in Jami Desktop

The Jami Desktop client uses API calls to retrieve information and assets required to display plugins on the Extension Store:

### APIs used

```{note}
The `x86_64-linux-gnu` architecture is an example of one, of many, valid architectures.
```

- **List of available plugins:**
`https://plugins.jami.net/?arch=x86_64-linux-gnu`
- **Download a plugin:**
`https://plugins.jami.net/download/x86_64-linux-gnu/PluginName`
- **Plugin version:**
`https://plugins.jami.net/versions/PluginName?arch=x86_64-linux-gnu`
- **Plugin icons:**
`https://plugins.jami.net/icons/PluginName?arch=x86_64-linux-gnu`
- **Plugin backgrounds:**
`https://plugins.jami.net/backgrounds/PluginName?arch=x86_64-linux-gnu`

### Display

The client uses this data to dynamically build the plugin store interface in the Desktop client.
When a user clicks “Install,” the client downloads the plugin via the API.

----------

## Important: Test locally before publication

**Before publishing an extension, test the extension on a local server.**
A poorly parsed extension could cause a HTTP 500 Internal Server Error, breaking the in-built Extension Store for Jami Desktop users.

Test Jami Extensions locally at [Jami Extension Store](https://git.jami.net/savoirfairelinux/jami-plugins-store).

**Test all the API routes mentioned above using `localhost:3000` instead of `plugins.jami.net`.**
These routes are used by the Jami Desktop client and must be functional.

----------

### Modify server (Caution: This is the Production Server)

1. Commit the changes.
2. After review, if the commit is correct, it will be merged.
3. Once merged, the Jenkins pipeline will be triggered:
[Jenkins Job - jami-plugin-store-deploy](https://jenkins.jami.net/job/jami-plugin-store-deploy/).

This job performs the following tasks:

1. Rebuild the Docker container for the server.
2. Push the updated image to the Docker Hub.

```{note}
The server pulls this Docker image daily via a crontab at `00:33 UTC`.
```

----------

## Add an extension

1. **Upload an extension** to `jami-repovm-01.mtl.sfl` in:
`/srv/repository/ring/plugins/`.
Follow the directory structure below:
    ```
    plugins/
    └── PluginName
        ├── desktop
        │   └── PluginName.jpl
        └── android
            └── PluginName.jpl
    ```

2. **Automatic Propagation:**
A crontab runs every 10 minutes to synchronize the files with `jami-repovm-02.mtl.sfl`.

3. **Update plugins.jami.net:**
Once synchronized, execute the following script on `jami-repovm-02.mtl.sfl` to update `plugins.jami.net`:
    ```bash
    /usr/local/bin/updateplugincontainer.sh
    ```

This script is automatically triggered daily at `00:33 UTC` via `crontab`.

4. **Verify everything works:**
Launch a fresh Jami Desktop client with an architecture compatible with the extension.
Navigate to the Extension Store page by selecting in Jami “Open settings” → “Extensions”.
The extensions should be displayed under “Available” heading.
Select “Install” and then test. Ideally repeat the installation and test on devices with different architectures and operating systems/distributions.

----------

## Congratulations, the Jami Extension has been added and is available for Jami users!