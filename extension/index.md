# Extension manual

The Jami extension manual is a reference for Jami extension developers, documenting the various aspects of the Jami Extension API and architecture.
In September 2020, the Jami team added extensions as a call feature for GNU/Linux, Windows, macOS, and Android users.
This enables personalized call and chat experiences by using available extensions.
Additionally, anyone is also able to transform awesome ideas into brand new Jami extensions!

```{important}
Jami source code also uses the term “plugin” to refer to “extension”.
```

```{toctree}
:maxdepth: 1

how-it-works
how-to-build
how-to-publish
creating-jami-extensions
jami-extension-certificate
extension-store-jami-client
tensorflow-extension
```