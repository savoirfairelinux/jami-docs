# Extension Store in the Jami client

```{note}
This page describes how the Jami Extension Store works and how to use it.
```

The Jami team has introduced extension as a call/chat feature for GNU/Linux, Windows, macOS, and Android, providing users with the ability to personalize their call/chat experience.
If you want to learn more about Jami extensions, please refer to the [Jami Extensions](how-it-work.md) page.

To enhance the extension ecosystem, the Jami team has decided to include a Jami Extension Store where they can distribute both their own extensions and third-party extensions.
However, before a third party can add an extension to the store, they must request a certificate signed by the certificate authority.
For more information, please see [Jami Extension Certificate](jami-extension-certificate.md).

The Jami daemon and client are responsible for verifying, installing, and managing new extensions in the device's environment.
Both the daemon and the client fetch new extensions, display them, verify their authenticity, and install them.
Jami already has the capability to display and install extensions.

## The Extension Store in the Daemon

The daemon is responsible for verifying each file of the extensions and determining whether the client can install it or not.

To verify the extension, it must contain the extension certificate and a file containing all the signatures of each file.

The content of the file is a representation of a map, where the path is the key and the hash of the target file is the value.
The client sends the path to the extension.

The daemon extracts the extension, reads the certificate, verifies the certificate chain, and checks if the certificate has been revoked by making a request to an OCSP server.

It then reads each file and its signature to verify the file.
If the certificate is verified and all signatures are valid, the extension can be installed.

## The Extension Store in the Client

The client is responsible for downloading extensions, communicating the path of the downloaded JPL file, displaying the Extension Store, and providing information about extensions.

The client sends requests to the Extension Store server to update information about the extensions, download extensions, and retrieve the list of available extensions.
It then displays the extensions according to the specification.

For example, a typical use case for the Extension Store is when the client wants to manage their extensions.
The client fetches all the information about the extensions and can request more details about a specific extension.

If the client decides to download an extension, it sends a request to the extension store API to obtain the extension resource.
The client then requests the daemon to install the extension.
The daemon verifies the extension using the certificate chain and the signatures of all files contained in the extension.

If the certificate and file signatures are verified correctly, the daemon proceeds with the installation.
If not, the extension archive is removed, and an error message is returned to the client.

![Interactive diagram of Jami Extension Store](images/jami-extension-store-interactive-diagram.png)

## Extension State

When a user wants to install an extension, a visual trigger should be displayed to show the stage of the extension installation.

The extension can be in one of three states: installed, installing, or downloading.
To handle all of these cases, a state machine is used to represent all the states of the extensions and the transitions between them.
The network manager and update manager for Windows have been refactored to handle all the states of the extension during the download process.

![Refactored NetworkManager and UpdateManager](images/jami-client-class-diagram.png)

A routine is triggered each day to check if an extension is not up-to-date.
The extension version manager allows extensions to be checked and updated if necessary.

![Jami Extension State Machine](images/jami-extension-state-machine.png)