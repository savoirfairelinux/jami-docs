## Development
### Transifex
This project uses Transifex as the translation service, you will need to install [Transifex command line tool](https://developers.transifex.com/docs/cli#download-from-github-releases-linuxmacwindows) for managing the translations. You can create a `.transifexrc` where you can include credentials related to you Transifex account. [Reference](https://developers.transifex.com/docs/using-the-client#global-settings) 
### Environment
Here are the packages for local development:
 - sphinx==6.2.1 (Required by sphinx-rtd-theme 1.2.2)
 - myst-parser==2.0.0
 - sphinx-rtd-theme==1.2.2


### Build HTML
Extract pot files:
```
make gettext 
```
Build HTML for the languages specified in a variable named `LANGS` in the Makefile. Make sure to `tx pull` the latest translations from Transifex prior building the HTML
```
make html 
```
### Pull/Update Languages 
To pull translated languages from Transifex:
```
tx pull -l <lang_code_1>,<lang_code_2> # Specify the language(s) to be pulled
tx --token <token> pull -l <lang_code_1>,<lang_code_2> # If the token value in `.transifexrc` didn't work
tx pull -a # all available languages at SFL Transifex
tx pull -l <lang_code_1>,<lang_code_2> --force # Force updating local '.po' files
```
### Push Translations/source Files to Transifex

> ***Pushing files to a remote could be dangerous, read [this](https://developers.transifex.com/docs/using-the-client#other-flags) carefully***

To push source files:
```
tx push --branch <your-branch> <project-slug>.<resource-slug>
```
To push translations:
```
tx push --branch <your-branch> <project-slug>.<resource-slug> -t # -t means only push the translation files
```
You can find the project slug and the resource slug at `.tx/config`. You can also add token before the command `push` if needed.

### Theme
This project uses `sphinx_rtd_theme` as the basic theme. You can find all the HTML files for this theme in the theme folder (e.g. `/python3.10/site-packages/sphinx_rtd_theme` if installed via PyPI). \
Here are the steps you may take to make changes to the theme:
1.  Find the HTML file that you need to change (`sphinx_rtd_theme/layout.html`)
2.  Create a file in the folder `_template` at the root directory with the same name. (`_template/layout.html`)
3.  Copy the content of the file from step 1 to the file you created in step 2.
4.  Make changes accordingly to the file (`_template/layout.html`) you created.

Having a file in `_template` with the same name makes Sphinx to override the default HTML file in the theme. Making changes to the original HTML files would do the same job but thoses files are not nor supposed to be tracked. You may also create your custom HTML file in `_template` and include it in other HTML files.

### View Built Pages
If the HTML files are built successfully, you can use any live server tool to serve the HTML files at your output folder. e.g. Live server extension at VS Code.

### Update Translations 
Always be careful when updating translation via API, TX Client, TM/MT autofill, File upload because it could potentially saved the errors in Transifex even the translation checks will run during the process. To identify the errors, you can filter the translations at Transifex translation editor. [Reference](https://help.transifex.com/en/articles/6241755-translation-checks-behavior) 
> Important: **Some problematic translations won't be interpreted as errors at Transifex but still can cause the build process to fail**. For example, the translations in `.po` files will replace the strings in `.rst` files respectively during the build. This makes having multiple `-` stacking in the translation files (`.po`) a potential issue, it could cause recursion error during the build because `-` is a special character in `.rst` files which is treated as a list item if placed at the line start. 

### Debugging Tips
#### Locate broken files 
You can try to exclude some files during the build process to locate the files that break the build. This can be achieved by adding file patterns in `conf.py`.
```
exclude_patterns = ['out/', 'manual/*.rst']
```
#### Build Logs
If you want to output the build log of `make html` to a desired file, go to `Makefile` at your root directory, find the code block with `%: Makefile`, then add the `-w` option like the following:
```
@for l in $(LANGS); do \
    $(CMD) -M $@ "$(SRC)" "$(OUT)/$$l" \
    	    -D language="$$l" \
    		-w "log-$$l.log" \ # Write the logs to this file where $$l is the value of l 
    	    -D html_baseurl="https://docs.jami.net/$$l" \
    	    $(OPTS); \
done	    
```